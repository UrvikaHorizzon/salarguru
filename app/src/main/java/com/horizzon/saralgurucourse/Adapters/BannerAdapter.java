package com.horizzon.saralgurucourse.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.horizzon.saralgurucourse.Models.BannerDetails;
import com.horizzon.saralgurucourse.R;

import java.util.ArrayList;

public class BannerAdapter extends BaseAdapter {

    private Context mCtx;
    private ArrayList<BannerDetails> heros;

    public BannerAdapter(Context mCtx, ArrayList<BannerDetails> heros){
        this.mCtx = mCtx;
        this.heros = heros;
    }
    @Override
    public int getCount() {
        return heros.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BannerDetails hero = heros.get(position);

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.flipper_items, null);
      //  TextView textView = (TextView) view.findViewById(R.id.textView);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
       // textView.setText(hero.getName());
        Glide.with(mCtx).load("http://nileshparmar.com/saralguru/uploads/banner/"+hero.getBannerImage()).into(imageView);
        return view;
    }
}