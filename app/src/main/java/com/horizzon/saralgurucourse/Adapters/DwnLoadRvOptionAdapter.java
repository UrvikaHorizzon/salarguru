package com.horizzon.saralgurucourse.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.JSONSchemas.GetCommentModel;
import com.horizzon.saralgurucourse.Models.DownloadVideoInQuality;
import com.horizzon.saralgurucourse.R;

import java.util.List;


public class DwnLoadRvOptionAdapter extends RecyclerView.Adapter<DwnLoadRvOptionAdapter.MyViewHolder> {

    Context mContext;
    private ItemClickListener onItemClickListener;
    List<DownloadVideoInQuality.Datum> dwnloadVideoQualityUrl;
    public DwnLoadRvOptionAdapter(Context context, List<DownloadVideoInQuality.Datum> dwnloadVideoQuality, ItemClickListener clickListener) {
        this.mContext = context;
        this.dwnloadVideoQualityUrl=dwnloadVideoQuality;
        onItemClickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rv_dwl_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DownloadVideoInQuality.Datum videoQualityModelList = dwnloadVideoQualityUrl.get(position);
        String allDetails = videoQualityModelList.getQuality() +  " "  + videoQualityModelList.getWidthByHeight() +"/"+videoQualityModelList.getFilesize();
        holder.txtQualityOption.setText(allDetails);
        holder.btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClick(view, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtQualityOption;
        ImageView btn_download;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtQualityOption = itemView.findViewById(R.id.txtQualityOption);
            btn_download = itemView.findViewById(R.id.btn_download);


        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
