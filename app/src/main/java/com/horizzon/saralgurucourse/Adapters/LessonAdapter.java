package com.horizzon.saralgurucourse.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.Activities.LessonActivity;
import com.horizzon.saralgurucourse.JSONSchemas.LessonSchema;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;
import com.horizzon.saralgurucourse.videocompressor.FileUtils;

import java.io.File;
import java.util.List;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {

    private static final String TAG = "LessonAdapter";
    private OnLessonClickListener mOnLessonClickListener;
    private onDownloadClickListner clickListner;
    boolean isFirstLesson, is_downloading;
    SharedPreferences sharedPreferences;
    private int row_index, mItemSelected;


    public class ViewHolder extends RecyclerView.ViewHolder implements LessonActivity.onDowenloadButtonPressedListener {
        private TextView mLessonTItle;
        private TextView mLessonDuration;
        private TextView mLessonSerialNumber;
        private CheckBox mLessonCompletionCheckbox;
        private ImageView Download_Button;
        private OnLessonClickListener onLessonClickListener;
        int selected_position = 0;

        public ViewHolder(@NonNull View itemView, final OnLessonClickListener onLessonClickListener) {
            super(itemView);
            this.mLessonTItle = itemView.findViewById(R.id.lessonTitle);
            this.mLessonDuration = itemView.findViewById(R.id.lessonDuration);
            this.mLessonSerialNumber = itemView.findViewById(R.id.serialNumber);
            this.Download_Button = itemView.findViewById(R.id.download_video_btn);
            this.onLessonClickListener = onLessonClickListener;
            this.mLessonCompletionCheckbox = itemView.findViewById(R.id.lessonCompletionCheckbox);
            ((LessonActivity) mContext).setOnBackPressedListener(this);
            Log.d("Coldplay", "ViewHolder Method");
        }

        @Override
        public void doBack(String title, int id) {
            if (mLesson.get(getAdapterPosition()).getTitle().equals(title)) {
                Download_Button.setImageResource(R.drawable.done_icon);
            }
        }
    }

    interface OnLessonClickListener {
        void onLessonClick(LessonSchema eachLesson, ViewHolder viewHolder);

        void onLessonCompletionListener(LessonSchema eachLesson, boolean lessonCompletionStatus);
    }


    interface onDownloadClickListner {
        void onClick(LessonSchema eachLesson, ViewHolder view, int position);
    }

    private List<LessonSchema> mLesson;
    private Context mContext;
    private int lessonCounter = 0;

    public LessonAdapter(Context context, List<LessonSchema> lesson, OnLessonClickListener onLessonClickListener, onDownloadClickListner clickListner, boolean isFirst, int lessonFrom) {
        this.mLesson = lesson;
        this.mContext = context;
        this.mOnLessonClickListener = onLessonClickListener;
        this.clickListner = clickListner;
        this.isFirstLesson = isFirst;
        this.lessonCounter = lessonFrom;
        sharedPreferences = mContext.getSharedPreferences(SaralGuru_Config.SHAREDPREFRANCE_VIDEO, Context.MODE_PRIVATE);
        sharedPreferences.registerOnSharedPreferenceChangeListener(mOnSharedPreferenceChangeListener);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_cell, parent, false);
        final LessonAdapter.ViewHolder holder = new LessonAdapter.ViewHolder(view, mOnLessonClickListener);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnLessonClickListener.onLessonClick(mLesson.get(holder.getAdapterPosition()), holder);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.mLessonSerialNumber.setText(Integer.toString(lessonCounter));
        lessonCounter++;
        if (isFirstLesson) {
            mOnLessonClickListener.onLessonClick(mLesson.get(holder.getAdapterPosition()), holder);
            isFirstLesson = false;
        }

        final LessonSchema currentLesson = mLesson.get(position);

        LessonSchema item = mLesson.get(position);
        holder.mLessonTItle.setText(item.getTitle());
        // Here I am just highlighting the background
        if (currentLesson.getLessonType().equals("other")) {
            holder.mLessonDuration.setText("📎 Attachment");
        } else if (currentLesson.getLessonType().equals("quiz")) {
            holder.mLessonDuration.setText("📝 Quiz");
        } else {
            holder.mLessonDuration.setText(currentLesson.getDuration());
        }
        if (currentLesson.getIsCompleted() == 1) {
            holder.mLessonCompletionCheckbox.setChecked(true);
        } else {
            holder.mLessonCompletionCheckbox.setChecked(false);
        }

        holder.mLessonCompletionCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnLessonClickListener.onLessonCompletionListener(currentLesson, holder.mLessonCompletionCheckbox.isChecked());
            }
        });

        /*String vidurl = currentLesson.getVideoUrl();
        String[] separated = vidurl.split("//");
        String first = separated[0];
        String second = separated[1];
        String[] separated1 = second.split("/");
        String FileName = separated1[2];
        String rootDir = FileUtils.getDirPath(mContext)  + File.separator +FileName;
        File fileEvents = new File(rootDir);
        if (fileEvents.exists()) {
            holder.Download_Button.setClickable(false);
            holder.Download_Button.setColorFilter(holder.Download_Button.getContext().getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }else {
            holder.Download_Button.setClickable(true);
        }*/

        holder.Download_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListner.onClick(currentLesson, holder, position);
            }
        });

        if (currentLesson.isSelected()) {
            holder.Download_Button.setColorFilter(mContext.getResources().getColor(R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return mLesson.size();
    }


    private final SharedPreferences.OnSharedPreferenceChangeListener mOnSharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            //TODO
            if (key.equals("downloading")) {
                is_downloading = sharedPreferences.getBoolean("downloading", false);
            }

            if (key.equals("position")) {
                row_index = sharedPreferences.getInt("position", 0);
                //Toast.makeText(mContext, ""+row_index, Toast.LENGTH_SHORT).show();
            }
        }
    };
}
