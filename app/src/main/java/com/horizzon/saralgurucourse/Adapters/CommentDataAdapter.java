package com.horizzon.saralgurucourse.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.JSONSchemas.GetCommentModel;
import com.horizzon.saralgurucourse.Models.CommentModelList;
import com.horizzon.saralgurucourse.R;

import java.util.ArrayList;
import java.util.List;

public class CommentDataAdapter extends RecyclerView.Adapter<CommentDataAdapter.MyViewHolder> {

    List<GetCommentModel.Datum> commentLists = new ArrayList<>();
    Context context;


    public CommentDataAdapter(Context ctx){
        this.context = ctx;
    }

    public CommentDataAdapter(Context ctx, List<GetCommentModel.Datum> commentListsData) {
        commentLists.clear();
        this.context = ctx;
        this.commentLists = commentListsData;
        notifyDataSetChanged();
    }

    public  void  updateList(Context ctx,List<GetCommentModel.Datum> commentList) {
        this.context = ctx;
        commentLists.clear();
        this.commentLists = commentList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CommentDataAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentDataAdapter.MyViewHolder holder, int position) {
        GetCommentModel.Datum commentModelList = commentLists.get(position);
        holder.txtCommentData.setText(commentModelList.getComment());
        //holder.txtCommentTime.setText(commentModelList.getTimeData());
    }



    @Override
    public int getItemCount() {
        return commentLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgUserProfileComment;
        TextView txtCommentData, txtCommentTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgUserProfileComment = itemView.findViewById(R.id.imgUserProfileComment);
            txtCommentTime = itemView.findViewById(R.id.txtCommentTime);
            txtCommentData = itemView.findViewById(R.id.txtCommentData);
        }
    }
}
