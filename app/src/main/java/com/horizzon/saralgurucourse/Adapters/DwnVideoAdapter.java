package com.horizzon.saralgurucourse.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.Database.VideoFileModel;
import com.horizzon.saralgurucourse.R;

import java.util.List;

public class DwnVideoAdapter extends RecyclerView.Adapter<DwnVideoAdapter.MyViewHolder> {

    Context mContext;
    List<VideoFileModel> videoFileModelList;
    OnItemClickListener onItemClickListener;

    public DwnVideoAdapter(Context context, List<VideoFileModel> videoFileModels, OnItemClickListener onItemClick) {
        this.mContext = context;
        this.videoFileModelList = videoFileModels;
        this.onItemClickListener=onItemClick;
    }

    @NonNull
    @Override
    public DwnVideoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_video_dwn, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DwnVideoAdapter.MyViewHolder holder, final int position) {
        final VideoFileModel videoFileModel=videoFileModelList.get(position);
        holder.txtDwnlesson.setText(videoFileModel.getVideoTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onClick(position,videoFileModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoFileModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtDwnlesson;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtDwnlesson = itemView.findViewById(R.id.txtDwnlesson);

        }
    }

    public interface OnItemClickListener{
        public void onClick(int poistion,VideoFileModel videoFileModel);
    }
}
