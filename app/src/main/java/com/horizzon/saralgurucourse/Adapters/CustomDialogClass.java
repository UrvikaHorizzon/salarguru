package com.horizzon.saralgurucourse.Adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.Database.VideoDataAsync;
import com.horizzon.saralgurucourse.Database.VideoFileModel;
import com.horizzon.saralgurucourse.Models.DownloadVideoInQuality;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.videocompressor.EncryptDecryptUtils;
import com.horizzon.saralgurucourse.videocompressor.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomDialogClass extends Dialog {

    public Activity c;
    public Dialog dialog;
    public RecyclerView rvDwnLoadSelection;
    boolean result;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    String FileName, filePath, urlStr;
    int videoId;
    String VideoTitle, videoType;
    LinearLayout llCustomDialog;
    // DownloadTasks downloadTasks;
    List<DownloadVideoInQuality.Datum> getVideoQualityList = new ArrayList<>();
    int VideoId;
    String LessonTitleName;

    public interface ItemClickListenerClose {
        void onItemClick();
    }

    @Override
    public void onBackPressed() {
        // downloadTasks.cancel(true);
        super.onBackPressed();
    }

    public CustomDialogClass(Activity a, int Video,String lessonTitle) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.VideoId = Video;
        this.LessonTitleName = lessonTitle;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialod_rv);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getCourseObjectById();
        rvDwnLoadSelection = findViewById(R.id.rvDwnLoadSelection);
        notificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

        llCustomDialog = findViewById(R.id.llCustomDialog);
        //initBroadcastReceiver();
    }

    private void getCourseObjectById() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://nileshparmar.com/rkp_vimeo/vimeo/example/").addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<DownloadVideoInQuality> call = api.getVideoQuality(VideoId);
        call.enqueue(new Callback<DownloadVideoInQuality>() {
            @Override
            public void onResponse(Call<DownloadVideoInQuality> call, Response<DownloadVideoInQuality> response) {

                if (response.body() != null) {
                    DownloadVideoInQuality courseSchema = response.body();
                    getVideoQualityList.addAll(courseSchema.getData());

                    DwnLoadRvOptionAdapter mAdapter = new DwnLoadRvOptionAdapter(getContext(), getVideoQualityList, new DwnLoadRvOptionAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Log.d("TAG", "onItemClick: " + position);
                            result = checkPermission();
                            if (ContextCompat.checkSelfPermission(c,
                                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                                ActivityCompat.requestPermissions(c, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

                            if (!isConnectingToInternet(c)) {
                                Toast.makeText(c, "Please Connect to Internet", Toast.LENGTH_LONG).show();
                            }
                            if (result) {
                                if (checkFolder()) {
                                    Log.d("TAG", "onItemClick: url :: " + courseSchema.getData().get(position).getUrl());
                                    String lessonTitle = LessonTitleName + "  " + courseSchema.getData().get(position).getQuality();
                                    download(courseSchema.getData().get(position).getUrl(), courseSchema.getData().get(position).getProfile(), lessonTitle, "html");
                                    dismiss();
                                }
                            }
                        }

                    });

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                    rvDwnLoadSelection.setLayoutManager(mLayoutManager);
                    rvDwnLoadSelection.setItemAnimator(new DefaultItemAnimator());
                    rvDwnLoadSelection.setAdapter(mAdapter);
                } else {
                    response.message();
                }

            }

            @Override
            public void onFailure(Call<DownloadVideoInQuality> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private boolean checkFolder() {
        File file = new File(c.getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/Saral_Guru/");
        if (!file.exists())
            file.mkdir();
        boolean isDirectoryCreated = file.exists();
        return isDirectoryCreated;
    }

    private void initBroadcastReceiver() {
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Intent downloadIntent = new Intent();
                downloadIntent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
                c.startActivity(downloadIntent);
            }
        };
        c.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(c, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(c, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(c);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(c, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(c, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(c, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(c);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(c, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);

                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(c, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    //Here you can check App Permission
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    private void download(String url, int id, String title, String videoTyp) {
      /*  DownloadTasks downloadTasks = new DownloadTasks(url, id, c, title, videoType);
        downloadTasks.execute();*/
        urlStr = url;
        videoId = id;
        VideoTitle = title;
        videoType = videoTyp;
        //downloadTasks=
        new DownloadTasks().execute(url);
    }

    public class DownloadTasks extends AsyncTask<String, String, String> {

        //int videoId;
        Context mContext;

        public DownloadTasks() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("id", "an", NotificationManager.IMPORTANCE_LOW);

                notificationChannel.setDescription("no sound");
                notificationChannel.setSound(null, null);
                notificationChannel.enableLights(false);
                notificationChannel.setLightColor(Color.BLUE);
                notificationChannel.enableVibration(false);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            notificationBuilder = new NotificationCompat.Builder(c, "id")
                    .setSmallIcon(android.R.drawable.stat_sys_download)
                    .setContentTitle("Download")
                    .setContentText("Lesson 3")
                    .setDefaults(0)
                    .setAutoCancel(false);
            notificationManager.notify(0, notificationBuilder.build());
        }

        public DownloadTasks(String url, int id, Context context, String title, String videoTypes) {
            urlStr = url;
            videoId = id;
            VideoTitle = title;
            videoType = videoTypes;
            mContext = context;

            //  dialog = new ProgressDialog(context);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("id", "an", NotificationManager.IMPORTANCE_LOW);

                notificationChannel.setDescription("no sound");
                notificationChannel.setSound(null, null);
                notificationChannel.enableLights(false);
                notificationChannel.setLightColor(Color.BLUE);
                notificationChannel.enableVibration(false);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            notificationBuilder = new NotificationCompat.Builder(c, "id")
                    .setSmallIcon(android.R.drawable.stat_sys_download)
                    .setContentTitle("Download")
                    .setContentText(title)
                    .setDefaults(0)
                    .setAutoCancel(true);
            notificationManager.notify(0, notificationBuilder.build());
        }

        int count = 1;

        @Override
        protected void onProgressUpdate(String... progress) {
            notificationBuilder.setOngoing(true);
            notificationBuilder.setProgress(100, Integer.parseInt(progress[0]), false);
            notificationBuilder.setContentText("Downloaded: " + progress[0] + "%");
            notificationManager.notify(0, notificationBuilder.build());
            super.onProgressUpdate(progress);
            //   Log.d("TAG", "onProgressUpdate: " + progress[0]);
        }

        @Override
        protected String doInBackground(String... sUrl) {
            File rootFile = null;
            String[] separated = urlStr.split("//");
            String first = separated[0];
            Log.d("TAG", "doInBackground: first : " + first);
            String second = separated[1];
            Log.d("TAG", "doInBackground: nn:" + second);
            String[] separated1 = second.split("/");
            FileName = separated1[7];
            Log.d("TAG", "doInBackground:FileName ::  " + FileName);
            FileUtils.deleteDownloadedFile(c, FileName);


            /*
             * Check url length and set in publishProgress
             * */
            try {
                URL url = new URL(sUrl[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                // OutputStream output = new FileOutputStream(new File(rootFile, FileName));
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");


                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            try {
                URL url = new URL(urlStr);
                rootFile = new File(FileUtils.getDirPath(c));
                rootFile.mkdir();
                filePath = rootFile.getPath();

                encrypt();
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                OkHttpClient client = new OkHttpClient.Builder()
                        .retryOnConnectionFailure(false)
                        .build();
                okhttp3.Response response = client.newCall(new Request.Builder()
                        .url(urlStr)
                        .get()
                        .build()).execute();

                FileOutputStream f = new FileOutputStream(new File(rootFile,
                        FileName));

                InputStream in = response.body().byteStream();
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (IOException e) {
                Log.d("Error....", e.toString());
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @SuppressLint("Restricte updateNotification(progress[0]);dApi")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (FileName != null) {
                Log.d("TAG", "onPostExecute: FileName :: " + FileName);
                final VideoFileModel videoFileModel = new VideoFileModel();
                videoFileModel.setVideoFile(FileName);
                videoFileModel.setVideoID(videoId);
                Log.d("TAG", "onPostExecute: filePath :: " + filePath);
                videoFileModel.setVideoPath(filePath);
                videoFileModel.setVideoTitle(VideoTitle);
                videoFileModel.setVideoType(videoType);
                VideoDataAsync videoDataAsync = new VideoDataAsync(videoFileModel, c);
                videoDataAsync.execute();
                updateNotification(100);
            }
        }
    }

    private void updateNotification(int currentProgress) {
        notificationBuilder.setProgress(100, currentProgress, false);
        notificationBuilder.setContentText("Downloaded: " + currentProgress + "%");
        notificationManager.notify(0, notificationBuilder.build());
        notificationBuilder.setAutoCancel(true);
        cancelNotification();
    }

    public void cancelNotification() {
        notificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0); // Notification ID to cancel
    }

    private void downloadfile(String vidurl) {
        String[] separated = vidurl.split("//");
        String first = separated[0];
        String second = separated[1];
        String[] separated1 = second.split("/");
        FileName = separated1[2];
        FileUtils.deleteDownloadedFile(c, FileName);
        try {
           /* notificationBuilder.setProgress(100, Integer.parseInt(progress[0]), false);
            notificationManager.notify(0,notificationBuilder.build());
           */
            URL url = new URL(vidurl);
            File rootFile = new File(FileUtils.getDirPath(c));
            rootFile.mkdir();
            filePath = rootFile.getPath();
            encrypt();
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();


            FileOutputStream f = new FileOutputStream(new File(rootFile,
                    FileName));

            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();

        } catch (IOException e) {
            Log.d("Error....", e.toString());
        }
    }

    private boolean encrypt() {

        try {
            byte[] fileData = FileUtils.readFile(FileUtils.getFilePath(c, FileName));
            byte[] encodedBytes = EncryptDecryptUtils.encode(EncryptDecryptUtils.getInstance(c).getSecretKey(), fileData);
            FileUtils.saveFile(encodedBytes, FileUtils.getFilePath(c, FileName));
            return true;
        } catch (Exception e) {
            Log.d("TAG", "encrypt: " + e.getMessage());
        }
        return false;
    }
}
