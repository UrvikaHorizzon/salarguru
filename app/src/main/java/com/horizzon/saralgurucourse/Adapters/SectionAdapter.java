package com.horizzon.saralgurucourse.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.videocompressor.EncryptDecryptUtils;
import com.horizzon.saralgurucourse.videocompressor.FileUtils;
import com.horizzon.saralgurucourse.JSONSchemas.LessonSchema;
import com.horizzon.saralgurucourse.Models.Section;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SectionAdapter extends RecyclerView.Adapter<SectionAdapter.ViewHolder> implements LessonAdapter.OnLessonClickListener, LessonAdapter.onDownloadClickListner {
    private static final String TAG = "SectionAdapter";
    private PassLessonToActivity mPassLessonToActivity;
    private onDownloadClick downloadClick;
    SharedPreferences sharedPreferences;

    @Override
    public void onLessonClick(LessonSchema eachLesson, LessonAdapter.ViewHolder viewHolder) {
        mPassLessonToActivity.PassLesson(eachLesson, viewHolder);
    }

    @Override
    public void onLessonCompletionListener(LessonSchema eachLesson, boolean lessonCompletionStatus) {
        mPassLessonToActivity.ToggleLessonCompletionStatus(eachLesson, lessonCompletionStatus);
    }

    @Override
    public void onClick(LessonSchema eachLesson, LessonAdapter.ViewHolder view, int postion) {
        String vidurl = eachLesson.getVideoUrl();
        String[] separated = vidurl.split("//");
        String first = separated[0];
        String second = separated[1];
        String[] separated1 = second.split("/");
        String FileName = separated1[2];
        String rootDir = FileUtils.getDirPath(mContext) + File.separator +FileName;
       File fileEvents = new File(rootDir);
        if (fileEvents.exists()) {
            Toast.makeText(mContext, "Already Other Video is Download", Toast.LENGTH_SHORT).show();
        }else {
            downloadClick.DowmlodClick(eachLesson, view, postion);
        }
    }
    private byte[] decrypt(String filename) {

        try {
            byte[] fileData = FileUtils.readFile(FileUtils.getFilePath(mContext,filename));
            byte[] decryptedBytes = EncryptDecryptUtils.decode(EncryptDecryptUtils.getInstance(mContext).getSecretKey(), fileData);
            return decryptedBytes;
        } catch (Exception e) {
            Log.d(TAG, "decrypt: " +  e.getMessage());
        }
        return null;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mSectionTItle, mNumberOfCompletedLessons, mNumberOfTotalLessons, mSectionNumberTitle;
        private RecyclerView mLessonRecyclerView;
        private LessonAdapter.OnLessonClickListener mOnLessonClickListener;
        private PassLessonToActivity passLessonToActivity;

        public ViewHolder(@NonNull View itemView, PassLessonToActivity passLessonIdToActivity) {
            super(itemView);
            this.mSectionTItle = itemView.findViewById(R.id.sectionTitle);
            this.mNumberOfCompletedLessons = itemView.findViewById(R.id.numberOfCompletedLessons);
            this.mLessonRecyclerView = itemView.findViewById(R.id.lessonRecyclerView);
            this.mNumberOfTotalLessons = itemView.findViewById(R.id.numberOfTotalLessons);
            this.mSectionNumberTitle = itemView.findViewById(R.id.sectionNumberTitle);
            this.passLessonToActivity = passLessonIdToActivity;

        }
    }

    private ArrayList<Section> mSection;
    boolean firstLessonFlag;
    private Context mContext;
    private ViewHolder mViewHolder;

    public SectionAdapter(Context context, ArrayList<Section> section, PassLessonToActivity passLessonActivity, onDownloadClick click, boolean firstLesson) {
        mContext = context;
        mSection = section;
        this.downloadClick = click;
        mPassLessonToActivity = passLessonActivity;
        firstLessonFlag = firstLesson;
        sharedPreferences = context.getSharedPreferences(SaralGuru_Config.SHAREDPREFRANCE_VIDEO, Context.MODE_PRIVATE);
        sharedPreferences.registerOnSharedPreferenceChangeListener(mOnSharedPreferenceChangeListener);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_cell, parent, false);
        final SectionAdapter.ViewHolder holder = new SectionAdapter.ViewHolder(view, mPassLessonToActivity);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Section currentSection = mSection.get(position);
        holder.mSectionTItle.setText(currentSection.getTitle());
        List<LessonSchema> mLessons = currentSection.getmLesson();
        LessonAdapter adapter = new LessonAdapter(mContext, mLessons, this, this, firstLessonFlag, currentSection.getLessonCounterStarts());
        firstLessonFlag = false;
        holder.mLessonRecyclerView.setAdapter(adapter);
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        holder.mLessonRecyclerView.setLayoutManager(manager);
        // this is hidden because of future work
        //holder.mNumberOfCompletedLessons.setText(Integer.toString(currentSection.getNumberOfCompletedLessons()));
        int section = position + 1;
//        holder.mSectionNumberTitle.setText("Section: "+section);
        holder.mNumberOfTotalLessons.setText(mLessons.size() + " Lessons");
        setHeight(mLessons.size(), holder.mLessonRecyclerView);
    }

    private void setHeight(int numberOfItems, RecyclerView mRecyclerView) {
        int pixels = Helpers.convertDpToPixel((numberOfItems * 90) + 10); // numberOfItems is the number of categories and the 90 is each items height with the margin of top and bottom. Extra 10 dp for readability
        ViewGroup.LayoutParams params1 = mRecyclerView.getLayoutParams();
        mRecyclerView.setMinimumHeight(pixels);
        mRecyclerView.requestLayout();
    }

    @Override
    public int getItemCount() {
        return mSection.size();
    }

    // This interface is responsible for passing the data to lesson activity
    public interface PassLessonToActivity {
        void PassLesson(LessonSchema eachLesson, LessonAdapter.ViewHolder viewHolder);

        int ToggleLessonCompletionStatus(LessonSchema eachLesson, boolean lessonCompletionStatus);
    }

    public interface onDownloadClick {
        void DowmlodClick(LessonSchema eachLesson, LessonAdapter.ViewHolder viewHolder, int position);
    }

    private final SharedPreferences.OnSharedPreferenceChangeListener mOnSharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            //TODO
        }
    };

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
           /* try {

                filePath = SiliCompressor.with(mContext).compressVideo(paths[0], paths[1]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }*/
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            File imageFile = new File(compressedFilePath);
            float length = imageFile.length() / 1024f; // Size in KB
            String value;
            if (length >= 1024) {
                value = length / 1024f + " MB";
                Log.d("TAG", "onPostExecute: if : " + value);
            } else {
                value = length + " KB";
                Log.d("TAG", "onPostExecute: else : " + value);
            }
            // String text = String.format(Locale.US, "%s\nName: %s\nSize: %s", getString(R.string.video_compression_complete), imageFile.getName(), value);

            Log.i("Silicompressor", "Path: " + compressedFilePath);
        }
    }
}
