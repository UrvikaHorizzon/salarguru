package com.horizzon.saralgurucourse.Fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.saralgurucourse.Activities.ExoVideoPlayer;
import com.horizzon.saralgurucourse.Activities.LessonActivity;
import com.horizzon.saralgurucourse.Adapters.DwnVideoAdapter;
import com.horizzon.saralgurucourse.Database.DatabaseClient;
import com.horizzon.saralgurucourse.Database.VideoFileModel;
import com.horizzon.saralgurucourse.Models.VIDEO_QUALITY;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.videocompressor.EncryptDecryptUtils;
import com.horizzon.saralgurucourse.videocompressor.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class DownloadedVideoFragment extends AppCompatActivity implements ExoVideoPlayer.PlaybackListener {

    // private ProgressBar progressBar;
    RecyclerView rvDwnVideo;
    TextView txtSelect;
    List<VideoFileModel> videoFileModels = new ArrayList<>();
    TextView txtNoData;
    View view;
    RecyclerView.LayoutManager mLayoutManager;
    RelativeLayout frameVideo;
    ExoVideoPlayer html5PlayerShow;
    VideoView vdVw;
    File existedVideoFile;
    VideoFileModel videoFileModel;
    RelativeLayout relaticeSecond;
    ImageButton backToMyCoursesList;
    public DownloadedVideoFragment() {
        // Required empty public constructor
    }


    public static DownloadedVideoFragment newInstance() {
        DownloadedVideoFragment fragment = new DownloadedVideoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_downloaded_video);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                backToMyCoursesList = findViewById(R.id.backToMyCoursesList);
                rvDwnVideo = findViewById(R.id.rvDwnVideo);
                txtNoData = findViewById(R.id.txtNoData);
                frameVideo = findViewById(R.id.rlOne);
                html5PlayerShow = findViewById(R.id.html5PlayerShow);
                vdVw =findViewById(R.id.vdVw);
                txtSelect=findViewById(R.id.txtSelect);
                relaticeSecond = findViewById(R.id.relaticeSecond);
                getDwnVideo();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        html5PlayerShow.pause();
        //vdVw.pause();
    }

    public void getDwnVideo() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                videoFileModels = DatabaseClient.getInstance(getApplication()).getAppDatabase()
                        .videoDao()
                        .getData();
                if (videoFileModels.size() > 0) {
                    final DwnVideoAdapter dwnVideoAdapter = new DwnVideoAdapter(getApplication(), videoFileModels, new DwnVideoAdapter.OnItemClickListener() {
                        @Override
                        public void onClick(final int poistion, VideoFileModel videoFileModel) {
                            String videopath = videoFileModels.get(poistion).getVideoPath() + File.separator + videoFileModels.get(poistion).getVideoFile();

                            if (html5PlayerShow.isPlaying()) {
                                html5PlayerShow.changeUrl(videopath);
                                txtSelect.setVisibility(View.GONE);
                            } else {
                                txtSelect.setVisibility(View.GONE);
                                playVideo(videoFileModel);
                            }
                        }
                    });

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLayoutManager = new LinearLayoutManager(getApplication());
                            rvDwnVideo.setLayoutManager(mLayoutManager);
                            rvDwnVideo.setItemAnimator(new DefaultItemAnimator());
                            rvDwnVideo.setAdapter(dwnVideoAdapter);
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            rvDwnVideo.setVisibility(View.GONE);
                            // fullScreenLessonShow.setVisibility(View.GONE);
                            frameVideo.setVisibility(View.GONE);
                            txtNoData.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });
    }

    public void playVideo(VideoFileModel videoFileModel) {
        // progressBar.setVisibility(View.VISIBLE);
        if (videoFileModel != null) {
            String rootDir = FileUtils.getDirPath(getApplication()) + File.separator + videoFileModel.getVideoFile();
            existedVideoFile = new File(rootDir);
            decrypt(rootDir);
            Log.d("TAG", "playVideo:existedVideoFile ::  " + existedVideoFile);
            if (existedVideoFile.exists()) {
                // progressBar.setVisibility(View.GONE);
                /*VIDEO_QUALITY quality = new VIDEO_QUALITY("0",
                        "1",
                        "2",
                        "3",
                        "4",
                        "5");*/
                //,quality
                html5PlayerShow.setMediaUrl(String.valueOf(Uri.fromFile(existedVideoFile)))
                        .enableAutoPlay(false)
                        .setOnPlaybackListener(this)
                        .build();
                //vdVw.setVideoPath(existedVideoFile);
                html5PlayerShow.play();

             /*  MediaController mediaController= new MediaController(this);
                mediaController.setAnchorView(vdVw);
                //Location of Media File
                Uri uri = Uri.parse(String.valueOf(existedVideoFile));
                //Starting VideView By Setting MediaController and URI
                vdVw.setMediaController(mediaController);
                vdVw.setVideoPath(String.valueOf(existedVideoFile));
                vdVw.setVideoURI(uri);
                vdVw.requestFocus();
                vdVw.start();*/
            } else {
                //progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplication(), "No video available.", Toast.LENGTH_SHORT).show();
            }
        } else {
            // progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplication(), "No video available.", Toast.LENGTH_SHORT).show();
        }
    }

    private byte[] decrypt(String filename) {

        try {
            byte[] fileData = FileUtils.readFile(FileUtils.getFilePath(getApplication(), filename));
            byte[] decryptedBytes = EncryptDecryptUtils.decode(EncryptDecryptUtils.getInstance(getApplication()).getSecretKey(), fileData);
            return decryptedBytes;
        } catch (Exception e) {
            Log.d("TAG", "decrypt: " + e.getMessage());
        }
        return null;
    }

    @Override
    public void onPlayEvent() {

    }

    @Override
    public void onPauseEvent() {

    }

    @Override
    public void onCompletedEvent() {

    }


    @Override
    public void onFullScreenEvent() {
        String oriantation = getScreenOrientation(getApplication());
        if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_PORTRAIT")
                || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
            //landscap
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            //potr
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public String getScreenOrientation(Context context) {
        final int screenOrientation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (screenOrientation) {
            case Surface.ROTATION_0:
                return "SCREEN_ORIENTATION_PORTRAIT";
            case Surface.ROTATION_90:
                return "SCREEN_ORIENTATION_LANDSCAPE";
            case Surface.ROTATION_180:
                return "SCREEN_ORIENTATION_REVERSE_PORTRAIT";
            default:
                return "SCREEN_ORIENTATION_REVERSE_LANDSCAPE";
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            screenLandScapMode();
        } else {
            screenPortraitMode();
        }
        html5PlayerShow.updateFullScreenIcon();
        adjustFullScreen(newConfig);
    }

    public void handleBackButton(View view) {
        html5PlayerShow.pause();
        DownloadedVideoFragment.super.onBackPressed();
    }

    private void screenLandScapMode() {
        backToMyCoursesList.setVisibility(View.GONE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        frameVideo.setVisibility(View.VISIBLE);
        relaticeSecond.setVisibility(View.GONE);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) frameVideo.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        html5PlayerShow.setMaxHeight(params.height);

    }

    private void screenPortraitMode() {
        relaticeSecond.setVisibility(View.VISIBLE);
        frameVideo.setVisibility(View.VISIBLE);
        rvDwnVideo.setVisibility(View.VISIBLE);
        if (true) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) frameVideo.getLayoutParams();
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.height = 600;
            frameVideo.setLayoutParams(params);
            html5PlayerShow.setMaxHeight(params.height);
        }
    }

    private void adjustFullScreen(Configuration config) {
        final View decorView = getWindow().getDecorView();
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            // Set the content to appear under the system bars so that the
                            // content doesn't resize when the system bars hide and show.
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            // Hide the nav bar and status bar
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } else {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

}