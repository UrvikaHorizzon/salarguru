package com.horizzon.saralgurucourse.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterViewFlipper;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.horizzon.saralgurucourse.Adapters.SlidingImage_Adapter;
import com.horizzon.saralgurucourse.Adapters.TopCourseAdapter;
import com.horizzon.saralgurucourse.JSONSchemas.CategorySchema;
import com.horizzon.saralgurucourse.JSONSchemas.CourseSchema;
import com.horizzon.saralgurucourse.Models.BannerDetails;
import com.horizzon.saralgurucourse.Models.Category;
import com.horizzon.saralgurucourse.Models.Course;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Adapters.CategoryAdapter;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CourseFragment extends Fragment {

    //vars
    private static final String TAG = "Fragment";
    // CategorySchema array of objects.
    private ArrayList<Category> mCategory = new ArrayList<>();
    // CourseSchema array of objects.
    private ArrayList<Course> mtopCourse = new ArrayList<>();

        private AdapterViewFlipper banner;
    private RecyclerView recyclerViewForTopCourses = null;
    private RecyclerView recyclerViewForCategories = null;
    private ProgressBar progressBar;
    CirclePageIndicator indicator;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<BannerDetails> imageModelArrayList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        //This function is responsible for fetching the images from the web
        View view = inflater.inflate(R.layout.course_fragment, container, false);
        banner  = view.findViewById(R.id.adperFlipper);

        recyclerViewForTopCourses = view.findViewById(R.id.recyclerViewForTopCourses);
        recyclerViewForCategories = view.findViewById(R.id.recyclerViewForCategories);
        mPager = view.findViewById(R.id.pager);
        indicator = view.findViewById(R.id.indicator);



        initProgressBar(view);
        Log.d("Done", "Start");
        getBanners();
        getTopCourses();
        getCategories();
//        setUpBanner();
        return view;
    }



    // Initialize the progress bar
    private void initProgressBar(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        Sprite circularLoading = new Circle();
        progressBar.setIndeterminateDrawable(circularLoading);
    }

    private void initTopCourseRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewForTopCourses.setLayoutManager(layoutManager);
        TopCourseAdapter adapter = new TopCourseAdapter(getActivity(), mtopCourse);
        recyclerViewForTopCourses.setAdapter(adapter);
    }

    private void initCategoryListRecyclerView() {
        CategoryAdapter adapter = new CategoryAdapter(getActivity(), mCategory);
        recyclerViewForCategories.setAdapter(adapter);
        recyclerViewForCategories.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void setHeight(int numberOfCategories) {

        int pixels = Helpers.convertDpToPixel((numberOfCategories * 90) + 10); // 9 is the number of categories and the 90 is each items height with the margin of top and bottom. Extra 10 dp for readability
        Log.d(TAG, "Lets change the height of recycler view");
        ViewGroup.LayoutParams params1 = recyclerViewForCategories.getLayoutParams();
        recyclerViewForCategories.setMinimumHeight(pixels);
        recyclerViewForCategories.requestLayout();
    }

    private void getTopCourses() {
        progressBar.setVisibility(View.VISIBLE);
        // Making an empty array of mtopCourse
        mtopCourse = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<List<CourseSchema>> call = api.getTopCourses();
        call.enqueue(new Callback<List<CourseSchema>>() {
            @Override
            public void onResponse(Call<List<CourseSchema>> call, Response<List<CourseSchema>> response) {
                Log.d("Size of response", response.toString());
                List<CourseSchema> topCourseSchema = response.body();
                for (CourseSchema m : topCourseSchema) {
                    mtopCourse.add(new Course(m.getId(), m.getTitle(), m.getThumbnail(), m.getPrice(), m.getInstructorName(), m.getRating(), m.getNumberOfRatings(), m.getTotalEnrollment(), m.getShareableLink(), m.getCourseOverviewProvider(),
                            m.getCourseOverviewUrl(),m.getDateAdded(),m.getDays()));
                }
                initTopCourseRecyclerView();
                Log.d("Course Done", "done");
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<CourseSchema>> call, Throwable t) {
                Log.d(TAG, "Top Course Fetched Failed");
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setUpBanner() {
        if (getActivity() != null) {
// Code goes here.
            mPager.setAdapter(new SlidingImage_Adapter(getActivity(),imageModelArrayList));
            indicator.setViewPager(mPager);
            final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
            indicator.setRadius(2 * density);

            NUM_PAGES =imageModelArrayList.size();

            Log.e("imagesize", String.valueOf(NUM_PAGES));

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    mPager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 10000, 10000);

            // Pager listener over indicator
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });

        }
    }

    private void getBanners() {
        progressBar.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<List<BannerDetails>> call =  api.getBanners();

        call.enqueue(new Callback<List<BannerDetails>>() {
            @Override
            public void onResponse(Call<List<BannerDetails>> call, Response<List<BannerDetails>> response) {
                if (response.isSuccessful()){
                    List<BannerDetails> rs=response.body();
                    imageModelArrayList=new ArrayList<>();
                    imageModelArrayList.addAll(rs);

//                    BannerAdapter adapter = new BannerAdapter(getActivity(),imageModelArrayList );
//
//                    //adding it to adapterview flipper
//                    banner.setAdapter(adapter);
//                    banner.setFlipInterval(1000);
//                    banner.startFlipping();

                    setUpBanner();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<List<BannerDetails>> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
//                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




        // Making an empty array of mtopCourse
      /*  mtopCourse = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.Banner_url).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<Banners> call =  api.getBanners();
        call.enqueue(new Callback<Banners>() {
            @Override
            public void onResponse(Call<Banners> call, Response<Banners> response) {
                ArrayList<Banner> heros  = response.body().getHeros();
                BannerAdapter adapter = new BannerAdapter(getActivity(), heros );

                //adding it to adapterview flipper
                banner.setAdapter(adapter);
                banner.setFlipInterval(1000);
                banner.startFlipping();
            }

            @Override
            public void onFailure(Call<Banners> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });*/


    }

    private void getCategories() {
        progressBar.setVisibility(View.VISIBLE);
        // Making empty array of category
        mCategory = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<List<CategorySchema>> call = api.getCategories();
        call.enqueue(new Callback<List<CategorySchema>>() {
            @Override
            public void onResponse(Call<List<CategorySchema>> call, Response<List<CategorySchema>> response) {
                Log.d(TAG, "CategorySchema Fetched Successfully");
                List<CategorySchema> categorySchema = response.body();
                for (CategorySchema m : categorySchema) {
                    mCategory.add(new Category(m.getId(), m.getName(), m.getThumbnail(), m.getNumberOfCourses()));
                }
                initCategoryListRecyclerView();
                setHeight(mCategory.size());
                Log.d("Done", " category done");
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<CategorySchema>> call, Throwable t) {
                Log.d(TAG, "CategorySchema Fetching Failed");
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }
}