package com.horizzon.saralgurucourse.Network;

import android.text.Editable;

import com.horizzon.saralgurucourse.JSONSchemas.CategorySchema;
import com.horizzon.saralgurucourse.JSONSchemas.CourseDetailsSchema;
import com.horizzon.saralgurucourse.JSONSchemas.CourseSchema;
import com.horizzon.saralgurucourse.JSONSchemas.GetCommentModel;
import com.horizzon.saralgurucourse.JSONSchemas.LanguageSchema;
import com.horizzon.saralgurucourse.JSONSchemas.LessonCompletionSchema;
import com.horizzon.saralgurucourse.JSONSchemas.MobileModel;
import com.horizzon.saralgurucourse.JSONSchemas.SectionSchema;
import com.horizzon.saralgurucourse.JSONSchemas.StatusSchema;
import com.horizzon.saralgurucourse.JSONSchemas.SystemSettings;
import com.horizzon.saralgurucourse.JSONSchemas.TotalVideoLikeCount;
import com.horizzon.saralgurucourse.JSONSchemas.UserDetails;
import com.horizzon.saralgurucourse.JSONSchemas.UserSchema;
import com.horizzon.saralgurucourse.JSONSchemas.UserVideoLike;
import com.horizzon.saralgurucourse.JSONSchemas.UserVideoLikeCheck;
import com.horizzon.saralgurucourse.Models.BannerDetails;
import com.horizzon.saralgurucourse.Models.DownloadVideoInQuality;
import com.horizzon.saralgurucourse.Models.PromoCodeModel;
import com.horizzon.saralgurucourse.Models.SendComment;
import com.horizzon.saralgurucourse.Models.referal_code;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Api {

    // API PREFIX FOR OFFLINE
    //String BASE_URL_PREFIX = "your_application_url_will_here";
    //String BASE_URL_PREFIX = "http://192.168.0.109/archive/products/acadpwd
    // emy/development";
    String BASE_URL_PREFIX = "http://www.nileshparmar.com/saralguru/";
    //String BASE_URL_PREFIX = "http://demo.academy-lms.com/mobile";

    // BASE URL
    String BASE_URL = BASE_URL_PREFIX + "/api/";

    // BASE URL FOR QUIZ
    String QUIZ_BASE_URL = BASE_URL_PREFIX + "/home/quiz_mobile_web_view/";

    // BASE URL FOR Banner
    String SIGN_UP_URL = BASE_URL_PREFIX + "/api/userdata/";

    // BASE URL FOR Banner
    String Banner_url = BASE_URL_PREFIX + "/home/banner";

    // Api call for fetching Top Courses
    @GET("top_courses")
    Call<List<CourseSchema>> getTopCourses();

    // Api call for fetching Categories
    @GET("categories")
    Call<List<CategorySchema>> getCategories();

    // Api call for fetching Category Wise Courses
    @GET("category_wise_course")
    Call<List<CourseSchema>> getCourses(@Query("category_id") int categoryId);

    // Api call for fetching Languages
    @GET("languages")
    Call<List<LanguageSchema>> getLanguages();

    // Api call for Filtering Courses
    @GET("filter_course")
    Call<List<CourseSchema>> getFilteredCourses(
            @Query("selected_category") String selectedCategory,
            @Query("selected_price") String selectedPrice,
            @Query("selected_level") String selectedLevel,
            @Query("selected_language") String selectedLanguage,
            @Query("selected_rating") String selectedRating,
            @Query("selected_search_string") String searchString);


    // Api call for fetching System banner
//    @GET("banner_image")
//    Call<Banners> getBanners();

    @GET("refferalcode")
    Call<List<referal_code>> getReferal_code(@Query("user_id") int id);


    @GET("banner")
    Call<List<BannerDetails>> getBanners();

    // Api call for fetching System Settings
    @GET("system_settings")
    Call<SystemSettings> getSystemSettings();

    @GET("register_mobile")
    Call<MobileModel> getMobile(@Query("mobile") String mobile,
                                @Query("device_id") String device_id);


    // Api call for fetching Course list from Search String
    @GET("courses_by_search_string")
    Call<List<CourseSchema>> getCoursesBySearchString(@Query("search_string") Editable searchString);

    // Api call for Login
    @GET("login")
    Call<UserSchema> getUserDetails(
            @Query("imei_no") String imei_no,
            @Query("email") String emailAddressForLogin);

    @GET("uservideolike")
    Call<UserVideoLike> getUserVideoLike(
            @Query("user_id") int user_id,
            @Query("video_id") int  video_id);

    @GET("uservideolikecheck")
    Call<UserVideoLikeCheck> getUserVideoLikeCheck(
            @Query("user_id") int user_id,
            @Query("video_id") int  video_id);


    @GET("totalvideolike")
    Call<TotalVideoLikeCount> getTotalvideolike(
            @Query("video_id") int  video_id);

    @GET("promocode_detail")
    Call<PromoCodeModel> sendPromoCode(
            @Query("title") String title,
            @Query("refferal_code") String refferal_code);

    // Api call for Fetching Logged In User Details
    @FormUrlEncoded
    @POST("sign_Up")
    Call<UserDetails> signUpDetail(
            @Field("auth_token") String auth_token,
            @Field("firstname") String firstName,
            @Field("lastname") String lastName,
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("refferal_by") String referalcode,
            @Field("imei_no") String imei_number);
//  @Field("password") String password,



    @FormUrlEncoded
    @POST("uservideo_comments")
    Call<SendComment> sendComment(
            @Field("user_id") int user_id,
            @Field("video_id") int video_id,
            @Field("comment") String comment);

//
   @GET("video_comment")
    Call<GetCommentModel> getCommentList(@Query("video_id") int  video_id);

    //
   /* @GET("a1.php")
    Call<GetCommentModel> getCommentList(@Query("video_id") int  video_id);*/

    @GET("a12.php")
    Call<DownloadVideoInQuality> getVideoQuality(@Query("video_id") int  video_id);


    // Api call for Fetching My Courses
    @GET("my_courses")
    Call<List<CourseSchema>> getMyCourses(@Query("auth_token") String authToken);

    // Api call for Fetching My Wishlist
    @GET("my_wishlist")
    Call<List<CourseSchema>> getMyWishlist(@Query("auth_token") String authToken);

    // Api call for Fetching My Wishlist
    @GET("toggle_wishlist_items")
    Call<StatusSchema> toggleWishListItems(@Query("auth_token") String authToken, @Query("course_id") int courseId);

    // Api call for Fetching My Wishlist
    @GET("sections")
    Call<List<SectionSchema>> getAllSections(@Query("auth_token") String authToken, @Query("course_id") int courseId);

    // Api call for Fetching Course Details
    @GET("course_details_by_id")
    Call<List<CourseDetailsSchema>> getCourseDetails(@Query("auth_token") String authToken, @Query("course_id") int courseId);

    // Api call for Fetching Course Details
    @GET("course_object_by_id")
    Call<CourseSchema> getCourseObject(@Query("course_id") int courseId);

    // Api call for Saving course progress with lesson completion status
    @GET("save_course_progress")
    Call<LessonCompletionSchema> saveCourseProgress(@Query("auth_token") String authToken, @Query("lesson_id") int lessonId, @Query("progress") int progress);

    @Multipart
    @POST("upload_user_image")
    Call<StatusSchema> uploadUserImage(
            @Part("auth_token") RequestBody authToken,
            @Part MultipartBody.Part userImage
    );

    // Api call for Fetching Logged In User Details
    @GET("userdata")
    Call<UserSchema> getUserProfileInfo(@Query("auth_token") String authToken);

    // Api call for Fetching Logged In User Details
    @FormUrlEncoded
    @POST("update_userdata")
    Call<UserSchema> updateUserData(
            @Field("auth_token") String auth_token,
            @Field("first_name") Editable firstName,
            @Field("last_name") Editable lastName,
            @Field("email") Editable email,
            @Field("biography") Editable biography,
            @Field("twitter_link") Editable twitterLink,
            @Field("facebook_link") Editable facebookLink,
            @Field("linkedin_link") Editable linkedInLink);

    // Api call for Fetching Logged In User Details
    @FormUrlEncoded
    @POST("update_password")
    Call<StatusSchema> updatePassword(
            @Field("auth_token") String auth_token,
            @Field("current_password") Editable currentPassword,
            @Field("new_password") Editable newPassword,
            @Field("confirm_password") Editable confirmPassword);


    @FormUrlEncoded
    @POST("enrol")
    Call<UserSchema> SubscribeCourses(@Field("user_id") String user_id,
                                      @Field("course_id") String course_id,
                                      @Field("payment_status") String payment_status,
                                      @Field("days") String days,
                                      @Field("date_added") String date_added
    );

    @GET()
    @Streaming
    Call<ResponseBody> downloadImage(@Url String fileUrl);
}

