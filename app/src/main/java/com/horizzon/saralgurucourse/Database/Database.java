package com.horizzon.saralgurucourse.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.horizzon.saralgurucourse.Models.video_details_db;

public class Database extends SQLiteOpenHelper {

    public static int DATABASE_VERSION = 1;
    public static String DATABASE_NAME = "SaralGuru";


    public static String VIDEO_PATH = "video_path";
    public static String ID = "id";
    public static String UID = "user_id";
    public static String VIDEO_ID = "course_id";
    public static String VIDEO_NAME = "video_name";
    public static String COUNT = "count";
    public static String TOTAL_COUNT = "total_count";
    public static final String TABLE_MAIN = "main_table";
    public static final String TABLE_VIDEO = "video_table";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_VIDEO_TABLE = "CREATE TABLE " + TABLE_VIDEO + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + UID + " INTEGER," +
                VIDEO_ID + " INTEGER," + VIDEO_NAME + " TEXT," + VIDEO_PATH + " TEXT"
                + ")";


        db.execSQL(CREATE_VIDEO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEO);
        onCreate(db);
    }


    public long store_video(video_details_db video) {
        String selectQuery = "SELECT *  FROM "
                + TABLE_MAIN;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(UID, video.getUid());
        values.put(VIDEO_ID, video.getVid_id());
        values.put(VIDEO_NAME, video.getVideo_name());
        values.put(VIDEO_PATH, video.getVideo_path());
        // insert row
        long scroll_id = db.insert(TABLE_VIDEO, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return scroll_id;
    }


    public video_details_db check_video(int id, String name) {

        video_details_db video = null;
        SQLiteDatabase db = this.getReadableDatabase();

        //String selectQuery = "SELECT * FROM " + TABLE_USER + " WHERE " + KEY_EMAIL + " =?" + emailId;
        String selectQuery = "SELECT * FROM " + TABLE_MAIN + " WHERE " + VIDEO_NAME + " =?";
        Cursor c = db.rawQuery("SELECT *  FROM " + TABLE_VIDEO + " WHERE " + VIDEO_ID + "=?" + " AND " + VIDEO_NAME + "=?", new String[]{String.valueOf(id), name});
        if (c != null) {
            c.moveToFirst();

            int vid_id = c.getInt(c.getColumnIndex(ID));
            int user_id = c.getInt(c.getColumnIndex(UID));
            int course_id = c.getInt(c.getColumnIndex(VIDEO_ID));
            String vid_name=c.getString(c.getColumnIndex(VIDEO_NAME));
            String vid_path=c.getString(c.getColumnIndex(VIDEO_PATH));

            video=new video_details_db(vid_id,user_id,course_id,vid_name,vid_path);

            c.close();
        }

        return video;

    }

}
