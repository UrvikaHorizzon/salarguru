package com.horizzon.saralgurucourse.Database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface VideoDao {

    @Insert
    void insert(VideoFileModel note);

    @Update
    void update(VideoFileModel note);

    @Query("SELECT * FROM VideoFile")
    List<VideoFileModel> getData();

  //  WHERE video_id =:VideoID  int VideoID
    @Delete
    void delete(VideoFileModel note);

    @Query("SELECT * FROM VideoFile WHERE video_id=:VideoID ")
    boolean findById(int VideoID);
}
