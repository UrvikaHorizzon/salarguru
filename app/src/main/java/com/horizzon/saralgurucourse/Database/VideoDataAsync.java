package com.horizzon.saralgurucourse.Database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

public class VideoDataAsync extends AsyncTask<Void, Void, Void> {
    VideoFileModel videoFileModelData;
    @SuppressLint("StaticFieldLeak")
    Context mContext;

    public VideoDataAsync(VideoFileModel videoFileModel, Context context) {
        videoFileModelData = videoFileModel;
        this.mContext=context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        VideoDao videoDao=DatabaseClient.getInstance(mContext).getAppDatabase()
                .videoDao();
       // if(!videoDao.findById(videoFileModelData.getVideoID())){
            DatabaseClient.getInstance(mContext).getAppDatabase()
                    .videoDao()
                    .insert(videoFileModelData);
        /*}else{
            DatabaseClient.getInstance(mContext).getAppDatabase()
                    .videoDao()
                    .update(videoFileModelData);
        }*/
        return null;
    }
}
