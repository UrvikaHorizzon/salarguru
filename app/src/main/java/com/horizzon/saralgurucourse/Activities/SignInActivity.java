package com.horizzon.saralgurucourse.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.horizzon.saralgurucourse.JSONSchemas.SystemSettings;
import com.horizzon.saralgurucourse.JSONSchemas.UserSchema;
import com.horizzon.saralgurucourse.Models.User;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignInActivity extends AppCompatActivity {

    EditText emailAddressForLogin, passwordForLogin;
    ImageView applicationLogo;
    TextView loginTitle;

    private ProgressBar progressBar;
    private User mUser;
    private static final String TAG = "SignInActivity";
    String imei_number = "", type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        init();
        initProgressBar();
        getSystemSettings();
    }

    private void init() {
        emailAddressForLogin = findViewById(R.id.emailAddressForLogin);
        passwordForLogin = findViewById(R.id.passwordForLogin);
        applicationLogo = findViewById(R.id.applicationLogo);
        loginTitle = findViewById(R.id.loginTitle);

        Log.e("imei_number",imei_number);
    }



    // Initialize the progress bar
    private void initProgressBar() {
        progressBar = findViewById(R.id.progressBar);
        Sprite circularLoading = new Circle();
        progressBar.setIndeterminateDrawable(circularLoading);
    }

    private void getSystemSettings() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<SystemSettings> call = api.getSystemSettings();
        call.enqueue(new Callback<SystemSettings>() {
            @Override
            public void onResponse(Call<SystemSettings> call, Response<SystemSettings> response) {
                SystemSettings systemSettings = response.body();
                Glide.with(getApplicationContext())
                        .asBitmap()
                        .load(systemSettings.getFavicon())
                        .into(applicationLogo);
                loginTitle.setText(systemSettings.getSystemName() + " Login");
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<SystemSettings> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                //Toast.makeText(SignInActivity.this, "Failed to fetch system data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*public void signUp(View view) {
        //  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Api.BASE_URL_PREFIX+"home/sign_up"));
        //  startActivity(browserIntent);

        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(intent);
    }*/

    /*public void logIn(View view) {
       *//* progressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        Api api = retrofit.create(Api.class);
        Call<UserSchema> call = api.getUserDetails( passwordForLogin.getText().toString(),Utility.getImeiNo(this),emailAddressForLogin.getText().toString());
        call.enqueue(new Callback<UserSchema>() {
            @Override
            public void onResponse(Call<UserSchema> call, Response<UserSchema> response) {
                UserSchema userSchema = response.body();

                if (userSchema.getValidity()==0){
                    Toast.makeText(SignInActivity.this, "Wrong Login Credentials", Toast.LENGTH_SHORT).show();
                }

                if (userSchema.getValidity() == 1) {
                    saveUserDataOnSharedPreference(userSchema);
                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserSchema> call, Throwable t) {
                Toast.makeText(SignInActivity.this, "An Error Occured"+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("error",t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });*//*
    }*/

    private void saveUserDataOnSharedPreference(UserSchema userSchema) {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Helpers.SHARED_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SaralGuru_Config.userID, userSchema.getUserId());
        editor.putInt(SaralGuru_Config.userValidity, userSchema.getValidity());
        editor.putString(SaralGuru_Config.userFirstName, userSchema.getFirstName());
        editor.putString(SaralGuru_Config.userLastName, userSchema.getLastName());
        editor.putString(SaralGuru_Config.userEmail, userSchema.getEmail());
        editor.putString(SaralGuru_Config.userRole, userSchema.getRole());
        editor.putString(SaralGuru_Config.userToken, userSchema.getToken());
        editor.putString(SaralGuru_Config.userPhoneNumber, userSchema.getMobile());
        editor.commit();
    }

//    private void getImeiNo() {
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            imei_number = Settings.Secure.getString(
//                    SignInActivity.this.getContentResolver(),
//                    Settings.Secure.ANDROID_ID);
//            Log.d("IMEI", imei_number);
//        }else {
//            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
//
//            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1111);
//            } else {
//                //TODO
//                TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
//                if (null != tm) {
//                    imei_number = tm.getDeviceId();
//                }
//                if (null == imei_number || 0 == imei_number.length()) {
//                    imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//                    Log.d("IMEI", imei_number);
//                }
////        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//                Log.d("IMEI", imei_number);
//            }
//        }
//        Log.d("IMEI", imei_number);
//    }
}
