package com.horizzon.saralgurucourse.Activities;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.CursorIndexOutOfBoundsException;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.horizzon.saralgurucourse.Database.Database;
import com.horizzon.saralgurucourse.Models.video_details_db;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.VideoConfig;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
public class FullScreenLessonPlayerActivity  extends YouTubeBaseActivity {
    private String  mVideoType;
    private String  mVideoUrl;
    YouTubePlayerView myouTubePlayerView;
    VideoView html5VideoPlayer;
    ImageView emptyVideoScreen;
    private ProgressBar progressBarForVideoPlayer;
    YouTubePlayer.OnInitializedListener  mOnInitializedListener;
    Database db;
    private int video_id;
    private String video_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fullscreen_activity);
        init();
        getTheLessonDetails();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width) , (int) (height));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        playLessonOnFullScreen(mVideoType, mVideoUrl);

    }

    private void init() {
        html5VideoPlayer = (VideoView) findViewById(R.id.html5Player);
        emptyVideoScreen = findViewById(R.id.emptyVideoScreen);
        db = new Database(this);
        initProgressBar();
    }

    // Initialize the progress bar
    private void initProgressBar() {
        progressBarForVideoPlayer = findViewById(R.id.loadingVideoPlayer);
        Sprite waveLoading = new Wave();
        progressBarForVideoPlayer.setIndeterminateDrawable(waveLoading);
    }

    private void getTheLessonDetails() {
        mVideoType = (String) getIntent().getSerializableExtra("videoType");
        mVideoUrl  = (String) getIntent().getSerializableExtra("videoUrl");
        video_id=getIntent().getIntExtra("video_id",0);
        video_name=getIntent().getStringExtra("video_name");
    }

    private void playLessonOnFullScreen(String videoType, String videoUrl) {
        if (videoType.equals("html5") && !videoUrl.equals("")){
            playHTML5Video(videoUrl);
        }else{
            Toast.makeText(this, "Proper Video URL is Missing", Toast.LENGTH_SHORT).show();
            html5VideoPlayer.setVisibility(View.GONE);
            emptyVideoScreen.setVisibility(View.VISIBLE);
        }
    }

    private void playHTML5Video(String videoUrl) {
        progressBarForVideoPlayer.setVisibility(View.VISIBLE);
        emptyVideoScreen.setVisibility(View.GONE);
        html5VideoPlayer.setVisibility(View.VISIBLE);
        html5VideoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                progressBarForVideoPlayer.setVisibility(View.INVISIBLE);

            }
        });


        try {
            video_details_db video = db.check_video(video_id,video_name);
            if (video != null && video.getVideo_path().length() > 0) {
                Toast.makeText(this, ""+video.getVideo_path(), Toast.LENGTH_SHORT).show();
                html5VideoPlayer.setVideoURI(Uri.parse(video.getVideo_path()));
                MediaController mediaController = new MediaController(this);
                html5VideoPlayer.setMediaController(mediaController);
                mediaController.setAnchorView(html5VideoPlayer);
                html5VideoPlayer.start();
            }
        } catch (CursorIndexOutOfBoundsException e) {

            html5VideoPlayer.setVideoPath(videoUrl);
            MediaController mediaController = new MediaController(this);
            html5VideoPlayer.setMediaController(mediaController);
            mediaController.setAnchorView(html5VideoPlayer);
            html5VideoPlayer.start();
            e.printStackTrace();
        }
//        html5VideoPlayer.setVideoPath(videoUrl);
//        MediaController mediaController = new MediaController(this);
//        html5VideoPlayer.setMediaController(mediaController);
//        mediaController.setAnchorView(html5VideoPlayer);
//        html5VideoPlayer.start();

        // This keeps tracks of if the video is buffering or not.
        final MediaPlayer.OnInfoListener onInfoToPlayStateListener = new MediaPlayer.OnInfoListener() {

            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                        progressBarForVideoPlayer.setVisibility(View.GONE);
                        return true;
                    }
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                        progressBarForVideoPlayer.setVisibility(View.VISIBLE);
                        return true;
                    }
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                        progressBarForVideoPlayer.setVisibility(View.GONE);
                        return true;
                    }
                }
                return false;
            }

        };
        html5VideoPlayer.setOnInfoListener(onInfoToPlayStateListener);
    }
    private void playYouTubeVideo(String videoUrl) {
        final String youtubeVideoId = VideoConfig.extractYoutubeId(videoUrl);
        myouTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubePlayer);
        myouTubePlayerView.setVisibility(View.VISIBLE);
        mOnInitializedListener = new YouTubePlayer.OnInitializedListener(){
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(youtubeVideoId);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        myouTubePlayerView.initialize(VideoConfig.getApiKey(), mOnInitializedListener);
    }

    private void playVimeoOnWebView(String videoUrl) {
        String vimeoVideoId = VideoConfig.extractVimeoId(videoUrl);
        WebView webView = (WebView) findViewById(R.id.videoPlayerWebView);
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/cache");
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDatabasePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/databases");
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl("https://player.vimeo.com/video/"+vimeoVideoId+"?player_id=player&title=0&byline=0&portrait=0&autoplay=1&api=1");
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("video_id",video_id);
        returnIntent.putExtra("video_name",video_name);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
