package com.horizzon.saralgurucourse.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.horizzon.saralgurucourse.Adapters.DwnVideoAdapter;
import com.horizzon.saralgurucourse.Database.DatabaseClient;
import com.horizzon.saralgurucourse.Database.VideoFileModel;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.VideoConfig;

import java.util.ArrayList;
import java.util.List;

public class DownloadedVideoActivity extends  AppCompatActivity {
    //YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener

    RecyclerView rvDwnVideo;
    List<VideoFileModel> videoFileModels = new ArrayList<>();
    DwnVideoAdapter dwnVideoAdapter;
    TextView txtNoData;
    private static final int RECOVERY_REQUEST = 1;
   // private YouTubePlayerView youTubeView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_downloaded_video);
        rvDwnVideo = findViewById(R.id.rvDwnVideo);
   /*     youTubeView = findViewById(R.id.player);
        youTubeView.initialize(VideoConfig.getApiKey(), this);*/
        //getDwnVideo();
    }

    public void getDwnVideo() {
        //txtDwnlesson item_video_dwn
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                videoFileModels = DatabaseClient.getInstance(getApplication()).getAppDatabase()
                        .videoDao()
                        .getData();
                Log.d("TAG", "run: " + videoFileModels.size());
                /*if (videoFileModels.size() > 0) {
                    dwnVideoAdapter = new DwnVideoAdapter(getApplicationContext(), videoFileModels);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    rvDwnVideo.setLayoutManager(mLayoutManager);
                    rvDwnVideo.setItemAnimator(new DefaultItemAnimator());
                    rvDwnVideo.setAdapter(dwnVideoAdapter);
                } else {
                    rvDwnVideo.setVisibility(View.GONE);

                }*/
            }
        });
    }

   /* @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo("https://www.youtube.com/watch?v=ia1iuXbEaYQ"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {

            Toast.makeText(this, errorReason.toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(VideoConfig.getApiKey(), this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }*/
}