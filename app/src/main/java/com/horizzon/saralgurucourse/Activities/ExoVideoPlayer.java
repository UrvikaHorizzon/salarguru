package com.horizzon.saralgurucourse.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.horizzon.saralgurucourse.Models.VIDEO_QUALITY;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.track_selection_dialog.TrackSelectionDialog;

public class ExoVideoPlayer extends LinearLayout implements AdapterView.OnItemSelectedListener, Player.EventListener, PlayerControlView.VisibilityListener{

    private static final String SHARED_PREF_NAME = "CUSTOM_VIDEO_PLAYER";
    private static final int PRIVATE_MODE = 0;
    private static final String KEY_VOLUME = "VOLUME_SETTINGS";
    float SPEED_NORMAL = 1f;
    float SPEED_MEDIUM = 1.25f;
    float SPEED_HIGH = 1.5f;
    float SPEED_HIGH_3 = 2f;

    float PITCH_MEDIUM = 1f;
    float PITCH_HIGH = 1f;
    float PITCH_HIGH_3 = 1f;

    private float playbackSpeed;

    private Context context;

    private SimpleExoPlayer exoPlayer;
    //  private ProgressBar progressBar;
    private ImageButton imagePlay;
    private ImageView ivFullScreen;
    private TextView exoSpeed;
    private Spinner exoquality;
    private ImageButton imagePause;

    private DefaultBandwidthMeter defaultBandwidthMeter;
    private VideoPlayerListener videoPlayerListener;

    private long playBackPosition = 0;

    private boolean autoPlay;
    private boolean autoMute;
    private boolean hideControllers;
    private boolean autoMuteSetByUser;

    private boolean playVideo;

    private PlaybackListener playbackListener;
    Uri videoUri;
    private int maxHeight;
    private int minHeight;

    private String mediaUrl;
    VIDEO_QUALITY quality;
    private SimpleExoPlayerView simpleExoPlayerView;
    private AudioManager audioManager;
    private DefaultTrackSelector trackSelector;
    private boolean isShowingTrackSelectionDialog;
    DefaultTrackSelector.SelectionOverride override;
    /* int HI_BITRATE = 2097152;
     int MI_BITRATE = 1048576;
     int LO_BITRATE = 524288;*/
    int HI_BITRATE = 2097152;
    int MI_BITRATE = 426 * 240;
    int LO_BITRATE = 144;


    public ExoVideoPlayer(Context context) {
        super(context);
        init(context);
    }

    public ExoVideoPlayer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ExoVideoPlayer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ExoVideoPlayer setMediaUrl(String mediaUrl) {
        //, VIDEO_QUALITY qualityData
      //  this.quality=qualityData;
      //  buildMediaSource(Uri.parse(mediaUrl));
        this.mediaUrl = mediaUrl;
        return this;
    }

    public ExoVideoPlayer enableAutoPlay(boolean autoPlay) {
        this.autoPlay = autoPlay;
        return this;
    }

    public ExoVideoPlayer enableAutoMute(boolean autoMute) {
        autoMuteSetByUser = true;

        this.autoMute = autoMute;
        return this;
    }

    public ExoVideoPlayer setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
        return this;
    }

    public ExoVideoPlayer setMinHeight(int minHeight) {
        this.minHeight = minHeight;
        return this;
    }

    public ExoVideoPlayer hideControllers(boolean hideControllers) {
        this.hideControllers = hideControllers;
        return this;
    }

    public ExoVideoPlayer setOnPlaybackListener(PlaybackListener playbackListener) {
        this.playbackListener = playbackListener;
        return this;
    }

    public void build() {
        initializeVideoPlayerView();
    }

    public boolean isPlaying() {
        return playVideo;
    }

    public long getCurrentTime() {
        return exoPlayer != null ? exoPlayer.getCurrentPosition() : 0;
    }

    public void stop() {
        releasePlayer();
    }

    public void pause() {
        pausePlayBack();
    }

    public void play() {
        startPlayBack();
    }

    public void setPlayBackPosition(long playBackPosition) {
        this.playBackPosition = playBackPosition;
    }

    private void init(Context context) {
        this.context = context;


        defaultBandwidthMeter = new DefaultBandwidthMeter();
        videoPlayerListener = new VideoPlayerListener();

        TrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(defaultBandwidthMeter);
        trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
     /*   trackSelector.setParameters(trackSelector.getParameters()
                .withMaxVideoBitrate(bitrate)
                .withMaxVideoSize(width, height));*/
        /*DefaultTrackSelector.Parameters parametersBuilder = new DefaultTrackSelector.ParametersBuilder()
                .setMaxAudioBitrate(144).build();
        trackSelector.setParameters(parametersBuilder);*/
       /* exoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context),
                new DefaultTrackSelector(trackSelectionFactory), new DefaultLoadControl());*/

        exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
        PlaybackParameters mPlaybackParameters = new PlaybackParameters(SPEED_NORMAL, SPEED_NORMAL);
        exoPlayer.setPlaybackParameters(mPlaybackParameters);
        playbackSpeed = SPEED_NORMAL;
        exoPlayer.addListener(videoPlayerListener);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth, parentHeight);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initializeVideoPlayerView() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_video_player, this, false);

            final ConstraintLayout constraintLayoutParent = view.findViewById(R.id.constraintLayoutParent);

            ViewGroup.LayoutParams layoutParams = constraintLayoutParent.getLayoutParams();
            layoutParams.width = this.getLayoutParams().width;
            layoutParams.height = this.getLayoutParams().height;

            if (minHeight != 0) {
                constraintLayoutParent.setMinHeight(minHeight);
            }

            if (maxHeight != 0) {
                constraintLayoutParent.setMaxHeight(maxHeight);
                layoutParams.height = maxHeight;
            }

            constraintLayoutParent.setLayoutParams(layoutParams);

            audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            simpleExoPlayerView = view.findViewById(R.id.simpleExoPlayerView);
            simpleExoPlayerView.setUseController(!hideControllers);
            simpleExoPlayerView.setControllerShowTimeoutMs(2000);

            simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            simpleExoPlayerView.hideController();
            simpleExoPlayerView.setOnClickListener(videoPlayerListener);

            //play button


          /*  progressBar = view.findViewById(R.id.progressBar);
            progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
*/
          /*  imageViewVolume = view.findViewById(R.id.imageViewVolume);
            imageViewVolume.setOnClickListener(videoPlayerListener);*/

            imagePlay = view.findViewById(R.id.exo_play);
            imagePlay.setOnClickListener(videoPlayerListener);

            ivFullScreen = view.findViewById(R.id.ivFullScreen);
            ivFullScreen.setOnClickListener(videoPlayerListener);


            exoSpeed = view.findViewById(R.id.exo_speed);
            exoSpeed.setOnClickListener(videoPlayerListener);

            exoquality = view.findViewById(R.id.exo_quality);
            imagePause = view.findViewById(R.id.exo_pause);
            imagePause.setOnClickListener(videoPlayerListener);

            exoquality.setVisibility(GONE);
            simpleExoPlayerView.setPlayer(exoPlayer);

            prepareVideoPlayer();
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                    R.array.quality_array, R.layout.spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            exoquality.setAdapter(adapter);
            exoquality.setOnItemSelectedListener(this);
            //exoquality.setSelection(1);

            this.addView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVisibilityChange(int visibility) {
        Log.d("visiblity", "check = " + visibility);
        exoquality.setVisibility(GONE);


    }
    public ExoVideoPlayer QualitySelection(FragmentManager fragmentManager) {

        exoquality.setOnClickListener(new OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {

                //simpleExoPlayerView.setResizeMode(144);
                //simpleExoPlayerView.s
              /* DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
                        .setMaxVideoBitrate(144)
                        .setForceHighestSupportedBitrate(true)
                        .build();
                trackSelector.setParameters(parameters);*/
                if (!isShowingTrackSelectionDialog && TrackSelectionDialog.willHaveContent(trackSelector)) {
                    isShowingTrackSelectionDialog = true;
                    TrackSelectionDialog trackSelectionDialog = TrackSelectionDialog.createForTrackSelector(trackSelector, dismissedDialog ->
                            isShowingTrackSelectionDialog = false
                    );
                    trackSelectionDialog.show(fragmentManager, null);
                }
                /*MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                if (mappedTrackInfo != null) {
                    CharSequence title = "Video";
                    int rendererIndex = 2;
                    int rendererType = mappedTrackInfo.getRendererType(rendererIndex);
                    boolean allowAdaptiveSelections =
                            rendererType == C.TRACK_TYPE_VIDEO
                                    || (rendererType == C.TRACK_TYPE_AUDIO
                                    && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                                    == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);
                   /* Pair<AlertDialog, TrackSelection> dialogPair =
                            TrackSelection.getDialog(this, title, trackSelector, rendererIndex);
                    dialogPair.second.setShowDisableOption(true);
                    dialogPair.second.setAllowAdaptiveSelections(allowAdaptiveSelections);
                    dialogPair.first.show();*//*

                    Pair<AlertDialog, TrackSelectionCustome> dialogPair1 = TrackSelectionCustome.getDialog(context, "Video", trackSelector, rendererIndex,getReso,
                    "L", exoPlayer.getVideoFormat());

                    dialogPair1.first.show();
                }*/
            }
        });
        return this;
    }

    public void changeUrl(String newMediaUrl) {
        this.mediaUrl = newMediaUrl;

        prepareVideoPlayer();
    }

    private void prepareVideoPlayer() {
        try {
            if (mediaUrl != null) {
                playBackPosition = 0;

                MediaSource mediaSource;
                if (mediaUrl.contains("file:")) {
                    mediaSource = prepareExoPlayerFromFileUri(Uri.parse(mediaUrl));
                } else {
                    mediaSource = buildMediaStore(mediaUrl);
                }

                exoPlayer.prepare(mediaSource, true, true);

                if (autoPlay) {
                    startPlayBack();
                }

               /* getVolumeSettings();
                manageMute();*/
            }
        } catch (Exception e) {
            e.printStackTrace();
            resetVideoPlayer();
        }
    }

    private MediaSource prepareExoPlayerFromFileUri(Uri uri) {

        DataSpec dataSpec = new DataSpec(uri);
        final FileDataSource fileDataSource = new FileDataSource();
        try {
            fileDataSource.open(dataSpec);
        } catch (FileDataSource.FileDataSourceException e) {
            e.printStackTrace();
        }

        DataSource.Factory factory = new DataSource.Factory() {
            @Override
            public DataSource createDataSource() {
                return fileDataSource;
            }
        };
        MediaSource audioSource = new ExtractorMediaSource(fileDataSource.getUri(),
                factory, new DefaultExtractorsFactory(), null, null);

        return audioSource;
    }


    private void resetVideoPlayer() {
        playVideo = false;
        playBackPosition = 0;
        //  progressBar.setVisibility(GONE);
        if (playbackListener != null) {
            playbackListener.onCompletedEvent();
        }
    }

   /* private void saveVolumeSettings() {
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(KEY_VOLUME, autoMute);
        sharedPreferencesEditor.apply();
    }

    private void getVolumeSettings() {
        if (!autoMuteSetByUser) {
            autoMute = sharedPreferences.getBoolean(KEY_VOLUME, false);
        }
    }*/

    private void pausePlayBack() {
        if (exoPlayer != null) {
            playVideo = false;

            if (playbackListener != null) {
                playbackListener.onPauseEvent();
            }

            playBackPosition = exoPlayer.getCurrentPosition();
            Log.d("TAG", "pausePlayBack: seek :  " + playBackPosition);
            exoPlayer.setPlayWhenReady(false);

        }
    }

    private void startPlayBack() {
        if (exoPlayer != null) {
            playVideo = true;

            if (playbackListener != null) {
                playbackListener.onPlayEvent();
            }

            exoPlayer.setPlayWhenReady(true);
            exoPlayer.seekTo(playBackPosition);
            Log.d("TAG", "startPlayBack: seek start :  " + playBackPosition);
        }
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            playVideo = false;

            context = null;

            audioManager.abandonAudioFocus(focusChangeListener);

            exoPlayer.stop();
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.removeListener(videoPlayerListener);
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private MediaSource buildMediaStore(String mediaUrl) {
         videoUri = Uri.parse(mediaUrl);

        DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory("custom_video_player", defaultBandwidthMeter, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
        DefaultExtractorsFactory extractorFactory = new DefaultExtractorsFactory();

        return new ExtractorMediaSource(videoUri, dataSourceFactory, extractorFactory, null, null);
    }

    /*public void updateFullScreenIcon() {
        String oriantation = getScreenOrientation(context);
        if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_PORTRAIT")
                || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
            ivFullScreen.setBackgroundResource(R.drawable.icon_full_screen);
        }
        else if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_LANDSCAPE")
                || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_LANDSCAPE")) {
            ivFullScreen.setBackgroundResource(R.drawable.ic_fullscreen_exit);
        }
       *//* else {
          //  ivFullScreen.setBackgroundResource(R.drawable.icon_full_screen);
        }*//*
    }*/

    public void updateFullScreenIcon() {
        String oriantation = getScreenOrientation(context);
        if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_PORTRAIT")
                || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
            ivFullScreen.setImageDrawable(context.getResources().getDrawable(R.drawable.exo_controls_fullscreen_enter));
        } else {
            ivFullScreen.setImageDrawable(context.getResources().getDrawable(R.drawable.exo_controls_fullscreen_exit));
        }
    }

    public String getScreenOrientation(Context context) {
        final int screenOrientation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (screenOrientation) {
            case Surface.ROTATION_0:
                return "SCREEN_ORIENTATION_PORTRAIT";
            case Surface.ROTATION_90:
                return "SCREEN_ORIENTATION_LANDSCAPE";
            case Surface.ROTATION_180:
                return "SCREEN_ORIENTATION_REVERSE_PORTRAIT";
            default:
                return "SCREEN_ORIENTATION_REVERSE_LANDSCAPE";
        }
    }

    private AudioManager.OnAudioFocusChangeListener focusChangeListener = new AudioManager.OnAudioFocusChangeListener() {

        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                    pause();
                    break;
                case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                    pause();
                    break;
                case (AudioManager.AUDIOFOCUS_LOSS):
                    pause();
                    break;
                case (AudioManager.AUDIOFOCUS_GAIN):
//                    play();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        long time = exoPlayer.getCurrentPosition();
        switch (pos) {
            case 0:
               // buildMediaSource(Uri.parse(quality.getZero()));
                exoPlayer.seekTo(0, time);
                break;
            case 1:
               //buildMediaSource(Uri.parse(quality.getOne()));
                exoPlayer.seekTo(0, time);
                break;
           /* case 2:
              //  buildMediaSource(Uri.parse(quality.getTwo()));
                exoPlayer.seekTo(0, time);
                break;
            case 3:
               //buildMediaSource(Uri.parse(quality.getThree()));
                exoPlayer.seekTo(0, time);
                break;
            case 4:
               // buildMediaSource(Uri.parse(quality.getFour()));
                exoPlayer.seekTo(0, time);
                break;
            case 5:
               // buildMediaSource(Uri.parse(quality.getFive()));
                exoPlayer.seekTo(0, time);
                break;*/
            default:
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class VideoPlayerListener implements OnClickListener, SeekBar.OnSeekBarChangeListener, ExoPlayer.EventListener, VideoRendererEventListener, AudioRendererEventListener {

        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            /*if (viewId == R.id.imageViewVolume) {
                playbackListener.onVolumeChange(autoMute);
                autoMute = !autoMute;
                manageMute();
            } else*/
            if (viewId == R.id.exo_play) {
                startPlayBack();
            } else if (viewId == R.id.exo_pause) {
                pausePlayBack();
            } else if (viewId == R.id.ivFullScreen) {
                updateFullScreenIcon();
                playbackListener.onFullScreenEvent();
            } else if (viewId == R.id.exo_speed) {
                if (playbackSpeed == SPEED_MEDIUM) {
                    PlaybackParameters playbackParameters = new PlaybackParameters(SPEED_HIGH, PITCH_HIGH);
                    exoPlayer.setPlaybackParameters(playbackParameters);
                    playbackSpeed = SPEED_HIGH;
                } else if (playbackSpeed == SPEED_HIGH) {
                    PlaybackParameters playbackParameters1 = new PlaybackParameters(SPEED_HIGH_3, PITCH_HIGH_3);
                    exoPlayer.setPlaybackParameters(playbackParameters1);
                    playbackSpeed = SPEED_HIGH_3;
                } else if (playbackSpeed == SPEED_HIGH_3) {
                    PlaybackParameters playbackParameters1 = new PlaybackParameters(SPEED_NORMAL, SPEED_NORMAL);
                    exoPlayer.setPlaybackParameters(playbackParameters1);
                    playbackSpeed = SPEED_NORMAL;
                } else {
                    PlaybackParameters playbackParameters2 = new PlaybackParameters(SPEED_MEDIUM, PITCH_MEDIUM);
                    exoPlayer.setPlaybackParameters(playbackParameters2);
                    playbackSpeed = SPEED_MEDIUM;
                }
                exoSpeed.setText(playbackSpeed + "x");
            }else if(viewId == R.id.exo_quality){
                buildMediaSource(videoUri);
            }
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

            startPlayBack();
        }

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {

        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            switch (playbackState) {
                case ExoPlayer.STATE_IDLE:

                    break;
                case ExoPlayer.STATE_READY:
                    simpleExoPlayerView.hideController();
                    playVideo = true;
                    //  progressBar.setVisibility(GONE);
                    break;
                case ExoPlayer.STATE_BUFFERING:
                   /* if (progressBar.getVisibility() != VISIBLE) {
                        simpleExoPlayerView.hideController();
                        progressBar.setVisibility(VISIBLE);
                    }*/
                    break;
                case ExoPlayer.STATE_ENDED:
                    resetVideoPlayer();
                    break;
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            resetVideoPlayer();
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {


        }

        @Override
        public void onAudioEnabled(DecoderCounters counters) {

        }

        @Override
        public void onAudioSessionId(int audioSessionId) {

        }

        @Override
        public void onAudioDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {

        }

        @Override
        public void onAudioInputFormatChanged(Format format) {

        }

        @Override
        public void onAudioSinkUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {

        }

        @Override
        public void onAudioDisabled(DecoderCounters counters) {

        }

        @Override
        public void onVideoEnabled(DecoderCounters counters) {

        }

        @Override
        public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {

        }

        @Override
        public void onVideoInputFormatChanged(Format format) {

        }

        @Override
        public void onDroppedFrames(int count, long elapsedMs) {

        }

        @Override
        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        }

        @Override
        public void onRenderedFirstFrame(Surface surface) {

        }

        @Override
        public void onVideoDisabled(DecoderCounters counters) {

        }

    }


    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                Util.getUserAgent(context, context.getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        MediaSource videoSource2 = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        ConcatenatingMediaSource concatenatedSource =
                new ConcatenatingMediaSource(videoSource, videoSource2);
        // Prepare the player with the source.
        exoPlayer.prepare(concatenatedSource);
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.addListener(this);
    }

    public interface PlaybackListener {


        void onPlayEvent();

        void onPauseEvent();

        void onCompletedEvent();

        void onFullScreenEvent();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                exoquality.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                // Activate the force enable
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                exoquality.setVisibility(View.GONE);

                break;
            default:
                // status = PlaybackStatus.IDLE;
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}