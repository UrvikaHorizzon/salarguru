package com.horizzon.saralgurucourse.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.internal.v;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horizzon.saralgurucourse.Adapters.CommentDataAdapter;
import com.horizzon.saralgurucourse.Adapters.CustomDialogClass;
import com.horizzon.saralgurucourse.Adapters.DwnLoadRvOptionAdapter;
import com.horizzon.saralgurucourse.Adapters.LessonAdapter;
import com.horizzon.saralgurucourse.Adapters.SectionAdapter;
import com.horizzon.saralgurucourse.Database.Database;
import com.horizzon.saralgurucourse.Database.VideoDataAsync;
import com.horizzon.saralgurucourse.Database.VideoFileModel;
import com.horizzon.saralgurucourse.JSONSchemas.CourseSchema;
import com.horizzon.saralgurucourse.JSONSchemas.GetCommentModel;
import com.horizzon.saralgurucourse.JSONSchemas.LessonCompletionSchema;
import com.horizzon.saralgurucourse.JSONSchemas.LessonSchema;
import com.horizzon.saralgurucourse.JSONSchemas.SectionSchema;
import com.horizzon.saralgurucourse.JSONSchemas.TotalVideoLikeCount;
import com.horizzon.saralgurucourse.JSONSchemas.UserVideoLike;
import com.horizzon.saralgurucourse.JSONSchemas.UserVideoLikeCheck;
import com.horizzon.saralgurucourse.Models.Course;
import com.horizzon.saralgurucourse.Models.CourseDetails;
import com.horizzon.saralgurucourse.Models.DownloadVideoInQuality;
import com.horizzon.saralgurucourse.Models.Lesson;
import com.horizzon.saralgurucourse.Models.MyCourse;
import com.horizzon.saralgurucourse.Models.Section;
import com.horizzon.saralgurucourse.Models.SendComment;
import com.horizzon.saralgurucourse.Models.VIDEO_QUALITY;
import com.horizzon.saralgurucourse.Models.video_details_db;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;
import com.horizzon.saralgurucourse.videocompressor.EncryptDecryptUtils;
import com.horizzon.saralgurucourse.videocompressor.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mbanje.kurt.fabbutton.FabButton;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LessonActivity extends AppCompatActivity implements SectionAdapter.PassLessonToActivity, SectionAdapter.onDownloadClick, ExoVideoPlayer.PlaybackListener {

    private TextView courseTitle;
    private TextView courseCompletionNumberOutOfTotal;
    private ProgressBar courseCompletionProgressBar;
    private MyCourse mCourse;
    private ArrayList<Section> mSections = new ArrayList<>();
    private ProgressBar progressBar;
    private ProgressBar progressBarForVideoPlayer;
    private ArrayList<CourseDetails> mEachCourseDetail = new ArrayList<>();

    RecyclerView sectionRecyclerView;
    ExoVideoPlayer exoVideoPlayer;
    LessonSchema mLesson;
    ImageView emptyVideoScreen;
    TextView attachmentTitle;
    Button downloadAttachmentButton;
    RelativeLayout downloadAttachmentArea;
    DownloadManager downloadManager;
    Spinner moreButton;
    RelativeLayout quizStuffs, download_view;
    TextView quizTitle;
    Button startQuiz;
    YouTubePlayer.OnInitializedListener mOnInitializedListener;
    FloatingActionButton floatingActionButton;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private DownloadTasks downloadTask = null;
    private AlertDialog dlgCancel = null;
    File file;
    String fileN = null;
    FabButton fabButton;
    Database db;
    SharedPreferences sharedPreferences;
    int i = 0;
    private onDowenloadButtonPressedListener onDowenloadButtonPressedListener;
    boolean is_downloading;
    String FileName, filePath, videoTypeOffline, VideoTitleOffLine;
    boolean result;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    File existedVideoFile;
    int videoIdOffLine;
    ImageButton backToMyCoursesList;
    RelativeLayout frameVideo, SecondView;
    FrameLayout frameLayout;
    private boolean screenChanges = false;
    private boolean screenRotationOption = false;
    LinearLayout LikeLL, CommentLL;
    RecyclerView RVComment;
    RelativeLayout llCommentRV;
    ImageView imgLike, imgCancel, imgSendData;
    TextView LikeCount;
    EditText edtCommentData;
    CommentDataAdapter commentDataAdapter;
    String Commentstr;
    // List<CommentModelList> commentLists = new ArrayList<>();
    List<GetCommentModel.Datum> getCommentModelList = new ArrayList<>();
    int lessonID, courseId;
    int UserId;
    VimeoPlayerView vimeoPlayer;

    ImageButton ivFullScreenViemo, ivFullScreenExitViemo;
    ImageView imgDwnVideo;
    List<DownloadVideoInQuality.Datum> getVideoQualityList = new ArrayList<>();
    RecyclerView rvDwnLoadSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = LessonActivity.this.getSharedPreferences(SaralGuru_Config.SHAREDPREFRANCE_VIDEO, Context.MODE_PRIVATE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lesson);

        vimeoPlayer = findViewById(R.id.vimeoPlayer);
        ivFullScreenViemo = findViewById(R.id.icFullScreen);
        ivFullScreenExitViemo = findViewById(R.id.icFullScreenExit);
        SecondView = findViewById(R.id.SecondView);
        llCommentRV = findViewById(R.id.llCommentRV);
        frameVideo = findViewById(R.id.lessonPlayer);
        frameLayout = findViewById(R.id.frameLayout);
        LikeLL = findViewById(R.id.LikeLL);
        CommentLL = findViewById(R.id.CommentLL);
        imgDwnVideo = findViewById(R.id.imgDwnVideo);
        rvDwnLoadSelection = findViewById(R.id.rvDwnLoadSelection);

        init();
        getCourseObject();
        getAllSections();
        initViewElement();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    private void init() {
        SharedPreferences prefs = getSharedPreferences(Helpers.SHARED_PREF, MODE_PRIVATE);
        UserId = prefs.getInt(SaralGuru_Config.userID, 0);
        backToMyCoursesList = findViewById(R.id.backToMyCoursesList);
        imgLike = findViewById(R.id.imgLike);
        imgCancel = findViewById(R.id.imgCancel);
        imgSendData = findViewById(R.id.imgSendData);
        LikeCount = findViewById(R.id.LikeCount);
        RVComment = findViewById(R.id.RVComment);
        edtCommentData = findViewById(R.id.edtCommentData);
        courseTitle = findViewById(R.id.courseTitle);
        courseCompletionNumberOutOfTotal = findViewById(R.id.courseCompletionNumberOutOfTotal);
        courseCompletionProgressBar = findViewById(R.id.courseCompletionProgressBar);
        sectionRecyclerView = findViewById(R.id.sectionRecyclerView);
        exoVideoPlayer = findViewById(R.id.vDetails);
        emptyVideoScreen = findViewById(R.id.emptyVideoScreen);
        downloadAttachmentArea = findViewById(R.id.downloadAttachmentArea);
        attachmentTitle = findViewById(R.id.attachmentTitle);
        downloadAttachmentButton = findViewById(R.id.downloadAttachmentButton);
        moreButton = findViewById(R.id.moreButton);
        quizTitle = findViewById(R.id.quizTitle);
        startQuiz = findViewById(R.id.startQuiz);
        quizStuffs = findViewById(R.id.quizStuffs);
        download_view = findViewById(R.id.download_view);
        floatingActionButton = findViewById(R.id.fab);
        fabButton = findViewById(R.id.download_btn);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        db = new Database(this);

        initProgressBar();
        initBroadcastReceiver();
    }


    private void initViewElement() {
        downloadAttachmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadAttachemt();
            }
        });
        initMoreOptionButton();
    }


    private void initMoreOptionButton() {
        ArrayAdapter<CharSequence> optionAdapter = ArrayAdapter.createFromResource(this, R.array.lesson_options, android.R.layout.simple_spinner_item);
        optionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        moreButton.setAdapter(optionAdapter);
        moreButton.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getItemIdAtPosition(i) == 1) {
                    getCourseObjectById("details");
                } else if (adapterView.getItemIdAtPosition(i) == 2) {
                    getCourseObjectById("share");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void userVideoLikeCount(int VideoId) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
            Api api = retrofit.create(Api.class);
            Call<TotalVideoLikeCount> call = api.getTotalvideolike(VideoId);
            call.enqueue(new Callback<TotalVideoLikeCount>() {
                @Override
                public void onResponse(Call<TotalVideoLikeCount> call, Response<TotalVideoLikeCount> response) {
                    TotalVideoLikeCount userSchema = response.body();
                    if (userSchema.getTotalcount() == 0) {
                        LikeCount.setText("( " + "0" + " )");
                        imgLike.setImageResource(R.drawable.ic_like);
                    }
                    if (userSchema.getTotalcount() > 0) {
                        LikeCount.setText("( " + String.valueOf(userSchema.getTotalcount()) + " )");
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<TotalVideoLikeCount> call, Throwable t) {
                    Toast.makeText(LessonActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("error", t.getMessage());
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LikeClick(int CourseId, int LessonId) {

        LikeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    progressBar.setVisibility(View.VISIBLE);
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();

                    Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
                    Api api = retrofit.create(Api.class);
                    Call<UserVideoLike> call = api.getUserVideoLike(UserId, LessonId);
                    call.enqueue(new Callback<UserVideoLike>() {
                        @Override
                        public void onResponse(Call<UserVideoLike> call, Response<UserVideoLike> response) {
                            UserVideoLike userSchema = response.body();

                            if (userSchema.getStatus() == 0) {
                                imgLike.setImageResource(R.drawable.ic_like);
                            }
                            if (userSchema.getStatus() == 1) {
                                imgLike.setImageResource(R.drawable.ic_like_fill);
                                userVideoLikeCount(LessonId);
                                ///Toast.makeText(LessonActivity.this, userSchema.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            progressBar.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onFailure(Call<UserVideoLike> call, Throwable t) {
                            Toast.makeText(LessonActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("error", t.getMessage());
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getLikeVideo(int LessonId) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
            Api api = retrofit.create(Api.class);
            Call<UserVideoLikeCheck> call = api.getUserVideoLikeCheck(UserId, LessonId);
            call.enqueue(new Callback<UserVideoLikeCheck>() {
                @Override
                public void onResponse(Call<UserVideoLikeCheck> call, Response<UserVideoLikeCheck> response) {
                    UserVideoLikeCheck userSchema = response.body();
                    if (userSchema.getStatus() == 0) {
                        imgLike.setImageResource(R.drawable.ic_like);
                    }
                    if (userSchema.getStatus() == 1) {
                        imgLike.setImageResource(R.drawable.ic_like_fill);
                        ///Toast.makeText(LessonActivity.this, userSchema.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<UserVideoLikeCheck> call, Throwable t) {
                    Toast.makeText(LessonActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("error", t.getMessage());
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void dwnVideoIdVise(int lessonID,String lessonTitle) {
        imgDwnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass cdd = new CustomDialogClass(LessonActivity.this,lessonID,lessonTitle);
                cdd.show();
            }
        });
    }

    public void CommentOnLec(int lessonID) {
        /*
         * get Comment list from server
         * */
        getCommentList(lessonID);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SecondView.setVisibility(View.VISIBLE);
                llCommentRV.setVisibility(View.GONE);
            }
        });
        CommentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SecondView.setVisibility(View.GONE);
                llCommentRV.setVisibility(View.VISIBLE);

                /*
                 * Send comment on server
                 * */
                SendCommentData(lessonID);
            }
        });
    }

    public void SendCommentData(int lessonID) {
        imgSendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edtCommentData.getText().toString().equals("") && !edtCommentData.getText().toString().isEmpty()) {
                    prepareSendData(edtCommentData.getText().toString(), lessonID);
                } else {
                    Toast.makeText(LessonActivity.this, "Enter your Comment...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void prepareSendData(String Commentstr, int lessonID) {

        /*SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy_HH-mm-ss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());*/
        try {
            progressBar.setVisibility(View.VISIBLE);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
            Api api = retrofit.create(Api.class);
            Call<SendComment> call = api.sendComment(UserId, lessonID, Commentstr);
            call.enqueue(new Callback<SendComment>() {
                @Override
                public void onResponse(Call<SendComment> call, Response<SendComment> response) {
                    SendComment userSchema = response.body();
                    if (userSchema.getStatus() == 1) {
                        GetCommentModel.Datum datum = new GetCommentModel.Datum();
                        datum.setComment(Commentstr);
                        getCommentModelList.add(getCommentModelList.size(), datum);
                        commentDataAdapter.notifyDataSetChanged();
                        edtCommentData.setText("");
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<SendComment> call, Throwable t) {
                    Toast.makeText(LessonActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("error", t.getMessage());
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCommentList(int lessonID) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            //http://nileshparmar.com/rkp_vimeo/vimeo/example/a1.php Api.BASE_URL
            Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
            Api api = retrofit.create(Api.class);
            Call<GetCommentModel> call = api.getCommentList(lessonID);
            call.enqueue(new Callback<GetCommentModel>() {
                @Override
                public void onResponse(Call<GetCommentModel> call, Response<GetCommentModel> response) {
                    GetCommentModel userSchema = response.body();
                    if (userSchema.getStatus() == 1) {
                        getCommentModelList.addAll(userSchema.getData());
                        /*
                         * Set data in adapter and set list
                         * */
                        commentDataAdapter = new CommentDataAdapter(getApplicationContext(), getCommentModelList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        RVComment.setLayoutManager(mLayoutManager);
                        RVComment.setItemAnimator(new DefaultItemAnimator());
                        RVComment.setAdapter(commentDataAdapter);
                       /* if (one == 1) {
                            commentDataAdapter.updateList(getApplicationContext(),getCommentModelList);
                        }*/
                    } else {
                        //Toast.makeText(LessonActivity.this, "No Data found.", Toast.LENGTH_SHORT).show();
                        RVComment.setVisibility(View.GONE);
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<GetCommentModel> call, Throwable t) {
                    Toast.makeText(LessonActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("error", t.getMessage());
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCourseObjectById(final String action) {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<CourseSchema> call = api.getCourseObject(mLesson.getCourseId());
        call.enqueue(new Callback<CourseSchema>() {
            @Override
            public void onResponse(Call<CourseSchema> call, Response<CourseSchema> response) {
                CourseSchema courseSchema = response.body();
                Course mCourse = new Course(courseSchema.getId(), courseSchema.getTitle(), courseSchema.getThumbnail(), courseSchema.getPrice(), courseSchema.getInstructorName(), courseSchema.getRating(), courseSchema.getNumberOfRatings(), courseSchema.getTotalEnrollment(), courseSchema.getShareableLink(), courseSchema.getCourseOverviewProvider(),
                        courseSchema.getCourseOverviewUrl(), courseSchema.getDateAdded(), courseSchema.getDays());
                if (action.equals("details")) {
                    switchToCourseDetailsActivity(mCourse);
                } else if (action.equals("share")) {
                    shareThisCourse(mCourse.getTitle(), mCourse.getShareableLink());
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CourseSchema> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void shareThisCourse(String courseTitle, String shareableLink) {
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.setType("text/plain");
        String shareBody = shareableLink;
        String shareSub = courseTitle;
        myIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
        myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(myIntent, "Share using"));
    }

    private void switchToCourseDetailsActivity(Course mCourse) {
        Intent intent = new Intent(LessonActivity.this, CourseDetailsActivity.class);
        intent.putExtra("Course", mCourse);
        this.startActivity(intent);
    }

    private void getAllSections() {
        progressBar.setVisibility(View.VISIBLE);
        // Making empty array of category
        mSections = new ArrayList<>();
        // Auth Token
        SharedPreferences preferences = getSharedPreferences(Helpers.SHARED_PREF, 0);
        String authToken = preferences.getString("userToken", "loggedOut");
        int course_id = mCourse.getId();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<List<SectionSchema>> call = api.getAllSections(authToken, mCourse.getId());
        call.enqueue(new Callback<List<SectionSchema>>() {
            @Override
            public void onResponse(Call<List<SectionSchema>> call, Response<List<SectionSchema>> response) {
                List<SectionSchema> sectionSchemas = response.body();
                for (SectionSchema m : sectionSchemas) {
                    mSections.add(new Section(m.getId(), m.getTitle(), m.getLessons(), m.getCompletedLessonNumber(), m.getTotalDuration(), m.getLessonCounterStarts(), m.getLessonCounterEnds()));
                }
                initSectionRecyclerView();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<SectionSchema>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    // Initialize the progress bar
    private void initProgressBar() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        Sprite circularLoading = new Circle();
        progressBar.setIndeterminateDrawable(circularLoading);

        progressBarForVideoPlayer = findViewById(R.id.loadingVideoPlayer);
        Sprite waveLoading = new Wave();
        progressBarForVideoPlayer.setIndeterminateDrawable(waveLoading);
    }

    private void initSectionRecyclerView() {
        SectionAdapter adapter = new SectionAdapter(LessonActivity.this, mSections, this, this, true);
        sectionRecyclerView.setAdapter(adapter);
        sectionRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void getCourseObject() {
        mCourse = (MyCourse) getIntent().getSerializableExtra("Course");
        courseTitle.setText(mCourse.getTitle());
        courseCompletionNumberOutOfTotal.setText(mCourse.getTotalNumberOfCompletedLessons() + "/" + mCourse.getTotalNumberOfLessons() + " Lessons are completed");
        courseCompletionProgressBar.setProgress(mCourse.getCourseCompletion());
        Log.d("CourseId", mCourse.getId() + "");
    }

    public void handleBackButton(View view) {
        exoVideoPlayer.pause();
        vimeoPlayer.pause();
        LessonActivity.super.onBackPressed();
    }


    @Override
    public void PassLesson(final LessonSchema eachLesson, LessonAdapter.ViewHolder viewHolder) {
        //viewHolder.itemView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_custom_blue_button));
        this.mLesson = eachLesson;
        courseTitle.setText(eachLesson.getTitle());
        if (eachLesson.getLessonType().equals("video")) {
            if (!isConnectingToInternet(this)) {
                String vidurl = eachLesson.getVideoUrl();
                videoTypeOffline = eachLesson.getVideoType();
                VideoTitleOffLine = eachLesson.getTitle();
                videoIdOffLine = eachLesson.getId();
                String[] separated = vidurl.split("//");
                String first = separated[0];
                String second = separated[1];
                String[] separated1 = second.split("/");
                String FileName = separated1[2];
                Log.d("TAG", "downloadfile: file : " + FileName);
                String rootDir = Environment.getExternalStorageDirectory()
                        + File.separator + "Saral_Guru" + File.separator + FileName;
                existedVideoFile = new File(rootDir);
                if (existedVideoFile.exists()) {
                    //html5VideoPlayer.setVideoPath(rootDir);
                    VideoPlayUrl(rootDir);
                    screenRotationOption = true;
                } else {
                    Toast.makeText(this, "No video available.", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (eachLesson.getVideoType().equals("html5") && !eachLesson.getVideoUrl().equals("")) {
                   /* if (exoVideoPlayer.isPlaying()) {
                        exoVideoPlayer.changeUrl(eachLesson.getVideoUrl());
                        lessonID = eachLesson.getId();
                        courseId = eachLesson.getCourseId();
                        *//*
                     * On Like Click
                     * *//*
                        LikeClick(courseId, lessonID);

                        *//*
                     * Check Video is like by user or not
                     * *//*
                        getLikeVideo(lessonID);
                        *//*
                     * Like count per video by all user
                     * *//*
                        userVideoLikeCount(eachLesson.getId());

                        *//*
                     * Comment get and post in this method
                     * *//*
                        CommentOnLec(lessonID);
                    } else {*/

                    this.getLifecycle().addObserver(vimeoPlayer);
                    vimeoPlayer.initialize(true, Integer.parseInt(eachLesson.getVideoUrl()));
                    dwnVideoIdVise(Integer.parseInt(eachLesson.getVideoUrl()),eachLesson.getTitle());
                    ivFullScreenViemo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //  updateFullScreenIcon();
                            ivFullScreenViemo.setVisibility(View.GONE);
                            ivFullScreenExitViemo.setVisibility(View.VISIBLE);
                            String oriantation = getScreenOrientation(LessonActivity.this);
                            if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_PORTRAIT")
                                    || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
                                //landscap
                                screenLandScapMode();
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                            }
                        }
                    });
                    ivFullScreenExitViemo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //  updateFullScreenIcon();
                            ivFullScreenViemo.setVisibility(View.VISIBLE);
                            ivFullScreenExitViemo.setVisibility(View.GONE);
                            String oriantation = getScreenOrientation(LessonActivity.this);
                            if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_LANDSCAPE")
                                    || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_LANDSCAPE")) {

                                screenPortraitMode();
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                            }
                        }
                    });
                    lessonID = eachLesson.getId();
                    courseId = eachLesson.getCourseId();
                    LikeClick(courseId, lessonID);
                    getLikeVideo(lessonID);
                    userVideoLikeCount(eachLesson.getId());
                    CommentOnLec(eachLesson.getId());
                } else {
                    Toast.makeText(this, "Proper Video URL is Missing", Toast.LENGTH_SHORT).show();
                    exoVideoPlayer.setVisibility(View.GONE);
                    quizStuffs.setVisibility(View.GONE);
                    emptyVideoScreen.setVisibility(View.VISIBLE);
                }
            }
        } else if (eachLesson.getLessonType().equals("other")) {
            attachmentTitle.setText("Download " + mLesson.getAttachment());
            exoVideoPlayer.setVisibility(View.GONE);
            quizStuffs.setVisibility(View.GONE);
            downloadAttachmentArea.setVisibility(View.VISIBLE);
        } else if (eachLesson.getLessonType().equals("quiz")) {
            progressBarForVideoPlayer.setVisibility(View.VISIBLE);
            exoVideoPlayer.setVisibility(View.GONE);
            downloadAttachmentArea.setVisibility(View.GONE);
            quizStuffs.setVisibility(View.VISIBLE);
            quizTitle.setText(eachLesson.getTitle());
            startQuiz.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(LessonActivity.this, QuizActivity.class);
                    intent.putExtra("lessonId", eachLesson.getId());
                    startActivity(intent);
                }
            });
            progressBarForVideoPlayer.setVisibility(View.GONE);
        }
    }

    public void updateFullScreenIcon() {
        String oriantation = getScreenOrientation(LessonActivity.this);
        if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_PORTRAIT")
                || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
            ivFullScreenViemo.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.exo_controls_fullscreen_enter));
        } else {
            ivFullScreenViemo.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.exo_controls_fullscreen_exit));
        }
    }


    @Override
    public int ToggleLessonCompletionStatus(LessonSchema eachLesson, final boolean lessonCompletionStatus) {
        int courseProgress = 0;
        if (lessonCompletionStatus) {
            courseProgress = 1;
        } else {
            courseProgress = 0;
        }
        progressBar.setVisibility(View.VISIBLE);
        // Auth Token
        SharedPreferences preferences = getSharedPreferences(Helpers.SHARED_PREF, 0);
        String authToken = preferences.getString("userToken", "loggedOut");
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<LessonCompletionSchema> call = api.saveCourseProgress(authToken, eachLesson.getId(), courseProgress);
        call.enqueue(new Callback<LessonCompletionSchema>() {
            @Override
            public void onResponse(Call<LessonCompletionSchema> call, Response<LessonCompletionSchema> response) {
                LessonCompletionSchema lessonCompletionSchema = response.body();
                courseCompletionNumberOutOfTotal.setText(lessonCompletionSchema.getNumberOfCompletedLessons() + "/" + lessonCompletionSchema.getNumberOfLessons() + " Lessons are completed");
                courseCompletionProgressBar.setProgress(lessonCompletionSchema.getCourseProgress());
                progressBar.setVisibility(View.GONE);
                if (lessonCompletionStatus) {
                    Toast.makeText(LessonActivity.this, "Marked as Completed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LessonActivity.this, "Marked as Incompleted", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LessonCompletionSchema> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(LessonActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });
        return 12;
    }

    private void playHTML5Video(final String videoUrl, final String title, final Integer course_id) {
        downloadAttachmentArea.setVisibility(View.GONE);
        progressBarForVideoPlayer.setVisibility(View.VISIBLE);
        emptyVideoScreen.setVisibility(View.GONE);
        //html5VideoPlayer.setVisibility(View.GONE);
        exoVideoPlayer.setVisibility(View.VISIBLE);
        quizStuffs.setVisibility(View.GONE);
        try {
            video_details_db video = db.check_video(course_id, title);
            if (video != null && video.getVideo_path().length() > 0) {
                VideoPlayUrl(video.getVideo_path());
            }
        } catch (CursorIndexOutOfBoundsException e) {
            VideoPlayUrl(videoUrl);
            e.printStackTrace();
        }
    }

    // This function is responsible for downloading an attachment
    public void downloadAttachemt() {
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(mLesson.getAttachmentUrl()));
        downloadManager.enqueue(request);
    }

    // Broadcast receiver is important for let you know that a service is done or ongoing or something else.
    private void initBroadcastReceiver() {
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Intent downloadIntent = new Intent();
                downloadIntent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
                startActivity(downloadIntent);
            }
        };
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    private boolean checkFolder() {
        file = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/Saral_Guru/");
        if (!file.exists())
            file.mkdir();
        boolean isDirectoryCreated = file.exists();
        return isDirectoryCreated;
    }

    @Override
    public void DowmlodClick(LessonSchema eachLesson, LessonAdapter.ViewHolder viewHolder, int position) {

        result = checkPermission();
        if (ContextCompat.checkSelfPermission(LessonActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(LessonActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

        if (!isConnectingToInternet(this)) {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
        }
        if (result) {
            if (checkFolder()) {
                SharedPreferences.Editor e = sharedPreferences.edit();
                e.putString("title", eachLesson.getTitle());
                e.putBoolean("downloading", true);
                e.commit();
                //download(eachLesson.getVideoUrl(), eachLesson.getId(), eachLesson.getTitle(), eachLesson.getVideoType());
            }
        }
    }

    private void getCourseObjectById() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://nileshparmar.com/rkp_vimeo/vimeo/example/").addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<DownloadVideoInQuality> call = api.getVideoQuality(459672272);
        call.enqueue(new Callback<DownloadVideoInQuality>() {
            @Override
            public void onResponse(Call<DownloadVideoInQuality> call, Response<DownloadVideoInQuality> response) {
                DownloadVideoInQuality courseSchema = response.body();
                getVideoQualityList.addAll(courseSchema.getData());

                DwnLoadRvOptionAdapter mAdapter = new DwnLoadRvOptionAdapter(getApplicationContext(), getVideoQualityList, new DwnLoadRvOptionAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Log.d("TAG", "onItemClick: " + position);

                        result = checkPermission();
                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                            ActivityCompat.requestPermissions(getParent(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

                        if (!isConnectingToInternet(getApplicationContext())) {
                            Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_LONG).show();
                        }
                        if (result) {
                            if (checkFolder()) {
                                Log.d("TAG", "onItemClick: url :: " + courseSchema.getData().get(position).getUrl());
                                download(courseSchema.getData().get(position).getUrl(), 0, "Lesson3", "html");
                                // dialog.dismiss();
                            }
                        }
                    }
                });
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                rvDwnLoadSelection.setLayoutManager(mLayoutManager);
                rvDwnLoadSelection.setItemAnimator(new DefaultItemAnimator());
                rvDwnLoadSelection.setAdapter(mAdapter);

            }

            @Override
            public void onFailure(Call<DownloadVideoInQuality> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    private void download(String url, int id, String title, String videoType) {
        DownloadTasks downloadTasks = new DownloadTasks(url, id, LessonActivity.this, title, videoType);
        downloadTasks.execute();
    }

    private void downloadfile(String vidurl) {
        String[] separated = vidurl.split("//");
        String first = separated[0];
        String second = separated[1];
        String[] separated1 = second.split("/");
        //  FileName = separated1[2];
        FileName = separated1[7];
        FileUtils.deleteDownloadedFile(this, FileName);


        try {
            URL url = new URL(vidurl);
            File rootFile = new File(FileUtils.getDirPath(this));
            rootFile.mkdir();
            filePath = rootFile.getPath();
            Log.d("TAG", "downloadfile: file path :: dwn : " + filePath);
            encrypt();
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            OkHttpClient client = new OkHttpClient.Builder()
                    .retryOnConnectionFailure(false)
                    .build();
            okhttp3.Response response = client.newCall(new Request.Builder()
                    .url(vidurl)
                    .get()
                    .build()).execute();

            FileOutputStream f = new FileOutputStream(new File(rootFile,
                    FileName));
            Log.d("TAG", "downloadfile: FileName :: dwn :" + FileName);
            //  InputStream in = c.getInputStream();
            InputStream in = response.body().byteStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (IOException e) {
            Log.d("Error.... bew data ::", e.toString());
        }
    }

    private boolean encrypt() {

        try {
            byte[] fileData = FileUtils.readFile(FileUtils.getFilePath(this, FileName));
            byte[] encodedBytes = EncryptDecryptUtils.encode(EncryptDecryptUtils.getInstance(this).getSecretKey(), fileData);
            FileUtils.saveFile(encodedBytes, FileUtils.getFilePath(this, FileName));
            return true;
        } catch (Exception e) {
            Log.d("TAG", "encrypt: " + e.getMessage());
        }
        return false;
    }

    public class DownloadTasks extends AsyncTask<String, String, String> {
        String urlStr, VideoTitle, videoType;
        int videoId;
        Context mContext;

        public DownloadTasks(String url, int id, Context context, String title, String videoTypes) {
            urlStr = url;
            videoId = id;
            VideoTitle = title;
            videoType = videoTypes;
            mContext = context;

            //  dialog = new ProgressDialog(context);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("id", "an", NotificationManager.IMPORTANCE_LOW);

                notificationChannel.setDescription("no sound");
                notificationChannel.setSound(null, null);
                notificationChannel.enableLights(false);
                notificationChannel.setLightColor(Color.BLUE);
                notificationChannel.enableVibration(false);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            notificationBuilder = new NotificationCompat.Builder(LessonActivity.this, "id")
                    .setSmallIcon(android.R.drawable.stat_sys_download)
                    .setContentTitle("Download")
                    .setContentText(title)
                    .setDefaults(0)
                    .setAutoCancel(true);
            notificationManager.notify(0, notificationBuilder.build());
        }

        int count = 1;

        @Override
        protected String doInBackground(String... sUrl) {
            try {
                URL url = new URL(sUrl[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                // OutputStream output = new FileOutputStream(new File(rootFile, FileName));
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");


                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            downloadfile(urlStr);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... progress) {
            notificationBuilder.setOngoing(true);
            notificationBuilder.setProgress(100, Integer.parseInt(progress[0]), false);
            notificationBuilder.setContentText("Downloaded: " + progress[0] + "%");
            notificationManager.notify(0, notificationBuilder.build());
            super.onProgressUpdate(progress);
        }

        @SuppressLint("Restricte updateNotification(progress[0]);dApi")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("DownloadTask", "Work Done! PostExecute");
            if (FileName != null) {
                final VideoFileModel videoFileModel = new VideoFileModel();
                videoFileModel.setVideoFile(FileName);
                videoFileModel.setVideoID(videoId);
                videoFileModel.setVideoPath(filePath);
                videoFileModel.setVideoTitle(VideoTitle);
                videoFileModel.setVideoType(videoType);
                VideoDataAsync videoDataAsync = new VideoDataAsync(videoFileModel, getApplicationContext());
                videoDataAsync.execute();
                updateNotification(100);
            }
        }
    }


    private void updateNotification(int currentProgress) {
        notificationBuilder.setProgress(100, currentProgress, false);
        notificationBuilder.setContentText("Downloaded: " + currentProgress + "%");
        notificationBuilder.setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SaralGuru_Config.FULLSCREEN_VIDEO) {
            if (resultCode == Activity.RESULT_OK) {
                int video_id = data.getIntExtra("video_id", 0);
                String video_name = data.getStringExtra("video_name");

                try {
                    video_details_db video = db.check_video(video_id, video_name);
                    if (video != null && video.getVideo_path().length() > 0) {
                        download_view.setVisibility(View.VISIBLE);
                        fabButton.setVisibility(View.GONE);
                        floatingActionButton.show();
                        VideoPlayUrl(video.getVideo_path());
                        floatingActionButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //  html5VideoPlayer.start();
                                VideoPlayUrl(video.getVideo_path());
                            }
                        });
                    }
                } catch (CursorIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        exoVideoPlayer.pause();
        vimeoPlayer.pause();

        super.onBackPressed();
    }


    public interface onDowenloadButtonPressedListener {
        void doBack(String title, int id);
    }

    public void setOnBackPressedListener(onDowenloadButtonPressedListener onBackPressedListener) {
        this.onDowenloadButtonPressedListener = onBackPressedListener;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(LessonActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(LessonActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LessonActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(LessonActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(LessonActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(LessonActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LessonActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(LessonActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);

                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(LessonActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }


    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void VideoPlayUrl(String url) {
        Activity activity = LessonActivity.this;
        if (url != null) {
            //html5VideoPlayer.setVisibility(View.GONE);
            exoVideoPlayer.setVisibility(View.VISIBLE);
            //Video quality list
            VIDEO_QUALITY quality = new VIDEO_QUALITY(url,
                    "https://www.hwpl.in/saralguru/l2640.mp4",
                    "http://www.iipv.org/saralguru/L3.mp4"
            );
            //, quality
            exoVideoPlayer.setMediaUrl(url)
                    .enableAutoPlay(false)
                    .setOnPlaybackListener(this)
                    .build();
            //exoVideoPlayer.QualitySelection(getSupportFragmentManager());
            progressBar.setVisibility(View.GONE);
            progressBarForVideoPlayer.setVisibility(View.GONE);
        } else {
            progressBarForVideoPlayer.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPlayEvent() {
    }

    @Override
    public void onPauseEvent() {
    }

    @Override
    public void onCompletedEvent() {

    }

    @Override
    public void onFullScreenEvent() {
        String oriantation = getScreenOrientation(LessonActivity.this);
        if (oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_PORTRAIT")
                || oriantation.equalsIgnoreCase("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
            //landscap
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            //potr
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

    }

    public String getScreenOrientation(Context context) {
        final int screenOrientation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (screenOrientation) {
            case Surface.ROTATION_0:
                return "SCREEN_ORIENTATION_PORTRAIT";
            case Surface.ROTATION_90:
                return "SCREEN_ORIENTATION_LANDSCAPE";
            case Surface.ROTATION_180:
                return "SCREEN_ORIENTATION_REVERSE_PORTRAIT";
            default:
                return "SCREEN_ORIENTATION_REVERSE_LANDSCAPE";
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        vimeoPlayer.reset();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            screenLandScapMode();
        } else {
            screenPortraitMode();
        }
        //exoVideoPlayer.updateFullScreenIcon();
        adjustFullScreen(newConfig);
    }

    private void screenLandScapMode() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        SecondView.setVisibility(View.GONE);
        backToMyCoursesList.setVisibility(View.GONE);
        frameVideo.setVisibility(View.VISIBLE);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) frameVideo.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        frameVideo.setLayoutParams(params);
        //  exoVideoPlayer.setMaxHeight(params.height);
        // vimeoPlayer.SetMaxHight(params.height);
    }

    private void screenPortraitMode() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        frameVideo.setVisibility(View.VISIBLE);
        SecondView.setVisibility(View.VISIBLE);
        download_view.setVisibility(View.GONE);
        DisplayMetrics metrics = new DisplayMetrics();
      /*  Display display = getWindowManager().getDefaultDisplay();
        int width=display.getWidth();
        int height=display.getHeight();*/
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) frameVideo.getLayoutParams();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = 0;
        // pxToDp(600);
        // dpToPx(250);
        //frameVideo.setLayoutParams(params);
        vimeoPlayer.SetMaxHight(params.height);
        // exoVideoPlayer.setMaxHeight(params.height);
            /*
            *  ViewGroup.LayoutParams layoutParams = constraintLayoutParent.getLayoutParams();
            layoutParams.width = this.getLayoutParams().width;
            layoutParams.height = this.getLayoutParams().height;
            *
            * */
    }

    public static int pxToDp(int px) {
        int dpl = (int) (px / Resources.getSystem().getDisplayMetrics().density);
        Log.d("TAG", "pxToDp: " + dpl);
        return dpl;
    }

    public static int dpToPx(int dp) {
        int pxl = (int) (dp * Resources.getSystem().getDisplayMetrics().density);
        Log.d("TAG", "dpToPx: " + pxl);
        return pxl;
    }

    private void adjustFullScreen(Configuration config) {
        final View decorView = getWindow().getDecorView();
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            // Set the content to appear under the system bars so that the
                            // content doesn't resize when the system bars hide and show.
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            // Hide the nav bar and status bar
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } else {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }
}