package com.horizzon.saralgurucourse.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;

public class LauncherActivity extends AppCompatActivity {
    public static int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */

                final SharedPreferences preferences = getSharedPreferences(Helpers.SHARED_PREF, 0);
                if (preferences != null) {
                    Intent mainIntent = new Intent(LauncherActivity.this, MainActivity.class);
                    LauncherActivity.this.startActivity(mainIntent);
                    LauncherActivity.this.finish();
                } else {
                    Intent mainIntent = new Intent(LauncherActivity.this, MobileActivity.class);
                    LauncherActivity.this.startActivity(mainIntent);
                    LauncherActivity.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
