package com.horizzon.saralgurucourse.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horizzon.saralgurucourse.JSONSchemas.SystemSettings;
import com.horizzon.saralgurucourse.JSONSchemas.UserDetails;
import com.horizzon.saralgurucourse.JSONSchemas.UserSchema;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;
import com.horizzon.saralgurucourse.Utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpActivity extends AppCompatActivity {

    EditText firstNameForSignUp, lastNameForSignUp, emailForSignUp, mobileNoForSignUp, passwordForSignUp, referalForSignup;
    ImageView applicationLogo;
    TextView loginTitle;
    Button createButton;
    String imei_number;
    String mobile;
    private ProgressBar progressBar;
    private static final String TAG = "SignUpActivity";
    private String firstName, LastName, email, MobileNumber, password, referalCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        Intent intent = getIntent();
        mobile = intent.getStringExtra("mobile");
        init();
        initProgressBar();
        getSystemSettings();
    }

    private void init() {
        firstNameForSignUp = findViewById(R.id.firstNameForSignUp);
        lastNameForSignUp = findViewById(R.id.lastNameForSignUp);
        emailForSignUp = findViewById(R.id.emailForSignUp);
        mobileNoForSignUp = findViewById(R.id.mobileNoForSignUp);
        mobileNoForSignUp.setClickable(false);
        mobileNoForSignUp.setText(mobile);
        passwordForSignUp = findViewById(R.id.pswdNoForSignUp);
        applicationLogo = findViewById(R.id.applicationLogo);
        loginTitle = findViewById(R.id.loginTitle);
        createButton = findViewById(R.id.signUpButton);
        referalForSignup = findViewById(R.id.txt_signup_referal);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName = firstNameForSignUp.getText().toString();
                LastName = lastNameForSignUp.getText().toString();
                email = emailForSignUp.getText().toString();
                MobileNumber = mobileNoForSignUp.getText().toString();
                password = passwordForSignUp.getText().toString();
                referalCode = referalForSignup.getText().toString();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if(firstName == null & firstName.isEmpty()){
                    Toast.makeText(SignUpActivity.this, "Please enter first name.", Toast.LENGTH_SHORT).show();
                }
                else  if(LastName == null & LastName.isEmpty()){
                    Toast.makeText(SignUpActivity.this, "Please enter last name.", Toast.LENGTH_SHORT).show();
                }
                else if(email == null & email.isEmpty()){
                    Toast.makeText(SignUpActivity.this, "Please enter email name.", Toast.LENGTH_SHORT).show();
                }
                else if(!email.matches(emailPattern) ){
                    Toast.makeText(SignUpActivity.this, "Please enter valid email name.", Toast.LENGTH_SHORT).show();
                }
                else if(MobileNumber == null & MobileNumber.isEmpty()){
                    Toast.makeText(SignUpActivity.this, "Please enter mobile number.", Toast.LENGTH_SHORT).show();
                }
                /*else if(password == null & password.isEmpty()){
                    Toast.makeText(SignUpActivity.this, "Please enter password number.", Toast.LENGTH_SHORT).show();
                }*/else {
                    submitSignUpForm();
                }

            }
        });

        imei_number = Utility.getImeiNo(SignUpActivity.this);
    }

    public void submit(View view) {
        submitSignUpForm();
    }

    private void initViewElementsWithUserInfo(UserDetails userDetails) {
        progressBar.setVisibility(View.VISIBLE);
        firstNameForSignUp.setText(userDetails.getFirstName());
        lastNameForSignUp.setText(userDetails.getLastName());
        emailForSignUp.setText(userDetails.getEmail());
        mobileNoForSignUp.setText(userDetails.getMobile());
        passwordForSignUp.setText(userDetails.getPassword());
        progressBar.setVisibility(View.INVISIBLE);
    }


    // Initialize the progress bar
    private void initProgressBar() {
        progressBar = findViewById(R.id.progressBar);
        Sprite circularLoading = new Circle();
        progressBar.setIndeterminateDrawable(circularLoading);
    }

    private void getSystemSettings() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<SystemSettings> call = api.getSystemSettings();
        call.enqueue(new Callback<SystemSettings>() {
            @Override
            public void onResponse(Call<SystemSettings> call, Response<SystemSettings> response) {
                SystemSettings systemSettings = response.body();
                Glide.with(getApplicationContext())
                        .asBitmap()
                        .load(systemSettings.getFavicon())
                        .into(applicationLogo);
                loginTitle.setText(systemSettings.getSystemName() + " Sign Up");
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<SystemSettings> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SignUpActivity.this, "Failed to fetch system data", Toast.LENGTH_SHORT).show();
            }
        });
    }

   /*
   * Sign Up api calling
   * */
    private void submitSignUpForm() {
        SharedPreferences preferences = this.getSharedPreferences(Helpers.SHARED_PREF, 0);
        String authToken = preferences.getString("userToken", "loggedOut");
        progressBar.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SIGN_UP_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);

        Log.d(TAG, "submitSignUpForm: ime no : " + Utility.getImeiNo(this));
        Call<UserDetails> call = api.signUpDetail(authToken, firstNameForSignUp.getText().toString(), lastNameForSignUp.getText().toString(), emailForSignUp.getText().toString(),
                mobileNoForSignUp.getText().toString(), referalForSignup.getText().toString(), Utility.getImeiNo(this));
        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                UserDetails userDetails = response.body();
                if (userDetails.getStatus().equals("User inserted successfully")) {
                    logIn(emailForSignUp.getText().toString(), passwordForSignUp.getText().toString());

                } else {
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(SignUpActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(SignUpActivity.this, "An Error Occured", Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*
    * Login API call for save data
    * */
    public void logIn(String email, String PSW) {
        progressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        Api api = retrofit.create(Api.class);
        Call<UserSchema> call = api.getUserDetails(Utility.getImeiNo(this), email);
        call.enqueue(new Callback<UserSchema>() {
            @Override
            public void onResponse(Call<UserSchema> call, Response<UserSchema> response) {
                UserSchema userSchema = response.body();
                if (userSchema.getValidity() == 0) {
                    Toast.makeText(SignUpActivity.this, "Wrong Login Credentials", Toast.LENGTH_SHORT).show();
                }
                if (userSchema.getValidity() == 1) {
                    saveUserDataOnSharedPreference(userSchema);
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserSchema> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("error", t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    /*
    * Save data in share preference
    * */
    private void saveUserDataOnSharedPreference(UserSchema userSchema) {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Helpers.SHARED_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SaralGuru_Config.userID, userSchema.getUserId());
        editor.putInt(SaralGuru_Config.userValidity, userSchema.getValidity());
        editor.putString(SaralGuru_Config.userFirstName, userSchema.getFirstName());
        editor.putString(SaralGuru_Config.userLastName, userSchema.getLastName());
        editor.putString(SaralGuru_Config.userEmail, userSchema.getEmail());
        editor.putString(SaralGuru_Config.userRole, userSchema.getRole());
        editor.putString(SaralGuru_Config.userToken, userSchema.getToken());
        editor.putString(SaralGuru_Config.userPhoneNumber, userSchema.getMobile());
        editor.commit();
    }
}
