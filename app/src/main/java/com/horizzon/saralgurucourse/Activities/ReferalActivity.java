package com.horizzon.saralgurucourse.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horizzon.saralgurucourse.JSONSchemas.SystemSettings;
import com.horizzon.saralgurucourse.JSONSchemas.UserSchema;
import com.horizzon.saralgurucourse.Models.PromoCodeModel;
import com.horizzon.saralgurucourse.Models.referal_code;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReferalActivity extends AppCompatActivity implements PaymentResultListener, View.OnClickListener {
    EditText UserName_txt, UserEmail_txt, User_Mobile_txt, Course_Name_txt, Course_Price_txt, Referal_Code_txt;
    private ProgressBar progressBar;
    ImageView applicationLogo;
    //    TextView loginTitle;
    private TextView txt_referal_code;
    private String TAG = " main";
    SharedPreferences preferences;
    private String username, useremail, user_phone;
    private int UserID, CourseID;

    private String dateAdded, days;
    private int buycourse = 0;
    private String course_title, course_price, type;
    Button payment_Button, btnApplyPromoCode;
    View payment_layout, referal_layout;
    double discountPrice;
    String rsSign;
    EditText txt_referal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referal);

        init();
        initProgressBar();
        getSystemSettings();
    }

    /*
     * initialization and declare
     * */
    private void init() {
        txt_referal = findViewById(R.id.txt_referal);
        progressBar = findViewById(R.id.progressBar);
        applicationLogo = findViewById(R.id.applicationLogo);
//        loginTitle = findViewById(R.id.loginTitle);
        txt_referal_code = findViewById(R.id.txt_refercode);
        UserName_txt = findViewById(R.id.firstNameForSignUp);
        UserEmail_txt = findViewById(R.id.emailForSignUp);
        User_Mobile_txt = findViewById(R.id.mobileNoForSignUp);
        Referal_Code_txt = findViewById(R.id.txt_signup_referal);
        Course_Name_txt = findViewById(R.id.CousrseNameForSignUp);
        Course_Price_txt = findViewById(R.id.CousrsePriceForSignUp);
        payment_Button = findViewById(R.id.payment_Button);
        payment_layout = findViewById(R.id.enter_referal_layout);
        referal_layout = findViewById(R.id.your_referal_laoyout);
        btnApplyPromoCode = findViewById(R.id.btnApplyPromoCode);
        btnApplyPromoCode.setOnClickListener(this);
        if (isLoggedIn()) {
            getReferal(getUserId());
            preferences = getApplicationContext().getSharedPreferences(Helpers.SHARED_PREF, 0);
            UserID = preferences.getInt(SaralGuru_Config.userID, 0);
            Log.d(TAG, "init: user Id : " + UserID);
            String fname = preferences.getString(SaralGuru_Config.userFirstName, "");
            String lname = preferences.getString(SaralGuru_Config.userLastName, "");
            useremail = preferences.getString(SaralGuru_Config.userEmail, "");
            user_phone = preferences.getString(SaralGuru_Config.userPhoneNumber, "");
            username = fname + " " + lname;

            if (getIntent().getExtras() != null) {
                course_title = getIntent().getStringExtra("title");
                course_price = getIntent().getStringExtra("course_price");
                CourseID = getIntent().getIntExtra("CourseID", 0);
                days = getIntent().getStringExtra("days");
                dateAdded = getIntent().getStringExtra("dateAdded");
                String[] separated = course_price.split(" ");
                rsSign = separated[0];

                type = getIntent().getStringExtra("type");

                if (type.equals("buy_course")) {
                    payment_layout.setVisibility(View.VISIBLE);
                    referal_layout.setVisibility(View.GONE);
                    UserName_txt.setText(username);
                    UserEmail_txt.setText(useremail);
                    User_Mobile_txt.setText(user_phone);
                    Course_Name_txt.setText(course_title);
                    Course_Price_txt.setText(course_price);
                }
            }
            payment_Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startPayment();
                }
            });
        }
    }

    /*
     * get referal code and get data from api
     * */
    private void getReferal(int userId) {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<List<referal_code>> call = api.getReferal_code(userId);
        call.enqueue(new Callback<List<referal_code>>() {
            @Override
            public void onResponse(Call<List<referal_code>> call, Response<List<referal_code>> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    List<referal_code> list = response.body();
                    referal_code code = list.get(0);

                    if (code.getRefferalCode().length() > 0) {
                        txt_referal_code.setText(code.getRefferalCode());
                    } else {
                        Toast.makeText(ReferalActivity.this, "referal code not available", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<referal_code>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ReferalActivity.this, "network failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    /*
     * Progressbar in circular
     * */
    private void initProgressBar() {
        Sprite circularLoading = new Circle();
        progressBar.setIndeterminateDrawable(circularLoading);
    }

    private void getSystemSettings() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<SystemSettings> call = api.getSystemSettings();
        call.enqueue(new Callback<SystemSettings>() {
            @Override
            public void onResponse(Call<SystemSettings> call, Response<SystemSettings> response) {
                SystemSettings systemSettings = response.body();
                Glide.with(getApplicationContext())
                        .asBitmap()
                        .load(systemSettings.getFavicon())
                        .into(applicationLogo);
//                loginTitle.setText(systemSettings.getSystemName()+" Sign Up");
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<SystemSettings> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ReferalActivity.this, "Failed to fetch system data", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private boolean isLoggedIn() {
        SharedPreferences preferences = this.getSharedPreferences(Helpers.SHARED_PREF, 0);
        int userValidity = preferences.getInt("userValidity", 0);
        if (userValidity == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void handleBackButton(View view) {
        ReferalActivity.super.onBackPressed();
    }

    private int getUserId() {
        SharedPreferences preferences = this.getSharedPreferences(Helpers.SHARED_PREF, 0);
        return preferences.getInt(SaralGuru_Config.userID, 0);
    }

    /*
     * Payment API calling
     * */
    public void startPayment() {
        /**
         * You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;
        final Checkout co = new Checkout();

        try {
            Double paymentvalue;
            progressBar.setVisibility(View.GONE);
            if (discountPrice > 0.0) {
                paymentvalue = discountPrice;
            } else {
                paymentvalue = Double.parseDouble(removeFirstChar(course_price));
            }
            JSONObject options = new JSONObject();
            options.put("name", SaralGuru_Config.appname);
            options.put("description", course_title);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://rzp-mobile.s3.amazonaws.com/images/rzp.png");
            options.put("currency", "INR");
            // amount is in paise so please multiple it by 100
            //Payment failed Invalid amount (should be passed in integer paise. Minimum value is 100 paise, i.e. ₹ 1)
            double total = Double.parseDouble(String.valueOf(paymentvalue));
            total = total * 100;
            options.put("amount", total);

            JSONObject preFill = new JSONObject();
//            preFill.put("name", username);
            preFill.put("contact", "91" + user_phone);
            preFill.put("email", useremail);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        // payment successfull pay_DGU19rDsInjcF2
        Log.e(TAG, " payment successfull " + s.toString());
        buycourse = 1;
        subscribeCourse(s);
    }

    private void subscribeCourse(String s) {
        progressBar.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<UserSchema> subscribe = api.SubscribeCourses(String.valueOf(UserID), String.valueOf(CourseID), s, days, dateAdded);

        subscribe.enqueue(new Callback<UserSchema>() {
            @Override
            public void onResponse(Call<UserSchema> call, Response<UserSchema> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body().getStatus().equals("User enroled successfully")) {

                        Toast.makeText(getApplicationContext(), "Payment successfully done", Toast.LENGTH_SHORT).show();
                        Intent mainIntent = new Intent(ReferalActivity.this, MainActivity.class);
                        mainIntent.putExtra(SaralGuru_Config.Status, SaralGuru_Config.PaymentSuccessful);
                        ReferalActivity.this.startActivity(mainIntent);
                        ReferalActivity.this.finish();
                        setFirstTime(true);
                    }
                }
            }

            @Override
            public void onFailure(Call<UserSchema> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ReferalActivity.this, "network failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPaymentError(int i, String s) {
        progressBar.setVisibility(View.GONE);
        Log.e(TAG, "error code " + String.valueOf(i) + " -- Payment failed " + s.toString());
        try {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(this, "Payment error please try again", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("OnPaymentError", "Exception in onPaymentError", e);
        }

    }

    public String removeFirstChar(String s) {
        return s.substring(1);
    }


    private void setFirstTime(boolean status) {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Helpers.SHARED_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("firsttime", status);
        editor.apply();
        editor.commit();
    }

    @Override
    public void onClick(View view) {
        String promoCode = Referal_Code_txt.getText().toString();
        String referalCode = txt_referal.getText().toString();

        if (view.getId() == R.id.btnApplyPromoCode) {
            if (!referalCode.isEmpty()) {
                if (!promoCode.isEmpty()) {
                    promoCode(promoCode,referalCode);
                } else {
                    Toast.makeText(this, "Please enter promo code.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Referral Code is Required for Apply Promocode", Toast.LENGTH_SHORT).show();
            }
        }
    }


    /*
     * Promo code Apply and check in api call && deduct amount and show price in percentage and fix value
     *
     * */
    private void promoCode(String promoCode,String referalCode) {
        progressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        Api api = retrofit.create(Api.class);
        Call<PromoCodeModel> call = api.sendPromoCode(promoCode,referalCode);
        call.enqueue(new Callback<PromoCodeModel>() {
            @Override
            public void onResponse(Call<PromoCodeModel> call, Response<PromoCodeModel> response) {
                PromoCodeModel userSchema = response.body();
                progressBar.setVisibility(View.INVISIBLE);
                if (userSchema.getValidity() == 1) {

                    Double paymentPrice = Double.parseDouble(removeFirstChar(course_price));
                    if (userSchema.getDetail().equals("percentage")) {
                        double pValue = Double.parseDouble(userSchema.getValue());
                        double diductamiunt = (paymentPrice * pValue) / 100;
                        discountPrice = paymentPrice - diductamiunt;
                        Course_Price_txt.setText(String.format("%s  %s", rsSign, discountPrice));
                    } else {
                        discountPrice = paymentPrice - Double.parseDouble(userSchema.getValue());
                        Course_Price_txt.setText(String.format("%s  %s", rsSign, discountPrice));
                    }
                } else {
                    Toast.makeText(ReferalActivity.this, "Please enter valid promo code and referal code.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PromoCodeModel> call, Throwable t) {
                Toast.makeText(ReferalActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("error", t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }
}