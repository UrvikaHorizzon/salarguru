package com.horizzon.saralgurucourse.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.horizzon.saralgurucourse.Database.VideoFileModel;
import com.horizzon.saralgurucourse.R;

import java.io.File;

public class DwnVideoShowActivity extends AppCompatActivity {

    VideoView html5PlayerShow;
    File existedVideoFile;
    VideoFileModel videoFileModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dwn_video_show);
        html5PlayerShow = findViewById(R.id.html5PlayerShow);
        html5PlayerShow.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int width, int height) {
                        /*
                         * add media controller
                         */
                        MediaController mc = new MediaController(DwnVideoShowActivity.this);
                        html5PlayerShow.setMediaController(mc);
                        /*
                         * and set its position on screen
                         */
                        mc.setAnchorView(html5PlayerShow);
                    }
                });
            }
        });

        videoFileModel = (VideoFileModel) getIntent().getSerializableExtra("videoFileModel");
        if (videoFileModel != null) {
            String rootDir = Environment.getExternalStorageDirectory()
                    + File.separator + "Saral_Guru" + File.separator + videoFileModel.getVideoFile();
            existedVideoFile = new File(rootDir);
            if (existedVideoFile.exists()) {
                html5PlayerShow.setVideoPath(rootDir);
                html5PlayerShow.start();
            }else {
                Toast.makeText(this, "No video available.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "No video available.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}