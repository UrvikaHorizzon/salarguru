package com.horizzon.saralgurucourse.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horizzon.saralgurucourse.JSONSchemas.MobileModel;
import com.horizzon.saralgurucourse.JSONSchemas.UserSchema;
import com.horizzon.saralgurucourse.Network.Api;
import com.horizzon.saralgurucourse.R;
import com.horizzon.saralgurucourse.Utils.Helpers;
import com.horizzon.saralgurucourse.Utils.SaralGuru_Config;
import com.horizzon.saralgurucourse.Utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MobileActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editTextMobile;
    Button buttonContinue;
    private ProgressBar progressBar;
    String imei_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile);
        Utility.getImeiNo(this);
        progressBar = findViewById(R.id.progressBar);
        editTextMobile = findViewById(R.id.editTextMobile);
        buttonContinue = findViewById(R.id.buttonContinue);
        buttonContinue.setOnClickListener(this);
        editTextMobile.requestFocus();
        editTextMobile.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager keyboard = (InputMethodManager) MobileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(editTextMobile.getWindowToken(), 0);
                    buttonContinue.setText("continue");
                    buttonContinue.setTextColor(getResources().getColor(R.color.white));
                    return true;
                }
                return false;
            }


        });
        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 10) {
                    buttonContinue.setText("continue");
                    buttonContinue.setTextColor(getResources().getColor(R.color.white));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.buttonContinue) {
            String mobile = editTextMobile.getText().toString().trim();
            if (mobile.isEmpty() || mobile.length() < 10) {
                editTextMobile.setError("Enter a valid mobile");
                editTextMobile.requestFocus();
                return;
            } else {
               // TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                mobileNumberVerifaction(mobile,Utility.getImeiNo(this));
            }
        }
    }

    /*
     * Mobile Number verification Api Calling
     * */
    private void mobileNumberVerifaction(final String mobile,String imei_number) {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Log.d("TAG", "mobileNumberVerifaction: mobile n : " + mobile);
        Log.d("TAG", "mobileNumberVerifaction: ime no : " + imei_number);
        Call<MobileModel> call = api.getMobile(mobile, imei_number);
        call.enqueue(new Callback<MobileModel>() {
            @Override
            public void onResponse(Call<MobileModel> call, Response<MobileModel> response) {
                progressBar.setVisibility(View.GONE);
                MobileModel systemSettings = response.body();
                if (systemSettings != null) {
                    if (systemSettings.getStatus() == 1) {
                        if (systemSettings.getMessage().equals("Login registered successfully")) {
                            Intent intent = new Intent(MobileActivity.this, phone_verify.class);
                            intent.putExtra("mobile", mobile);
                            startActivity(intent);
                            finish();
                        } else if (systemSettings.getMessage().equals("Login successfully")) {
                            /*
                             * when user is all ready regi that time fetch all details and save in prefrence .
                             * */
                            logInData(mobile);
                            finish();
                        }
                    } else if (systemSettings.getStatus() == 2) {
                        Intent intent = new Intent(MobileActivity.this, phone_verify.class);
                        intent.putExtra("mobile", mobile);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(MobileActivity.this, systemSettings.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MobileActivity.this, "No data.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MobileModel> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MobileActivity.this, "Failed to fetch system data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
     * Get Login Data from server and save in share Preference
     * */
    public void logInData(String email) {
        Log.d("TAG", "logInData: emi : " + Utility.getImeiNo(this));
        progressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        Api api = retrofit.create(Api.class);
        Call<UserSchema> call = api.getUserDetails(Utility.getImeiNo(this), email);
        call.enqueue(new Callback<UserSchema>() {
            @Override
            public void onResponse(Call<UserSchema> call, Response<UserSchema> response) {
                UserSchema userSchema = response.body();

                if (userSchema.getValidity() == 0) {
                    Toast.makeText(MobileActivity.this, "Wrong Login Credentials", Toast.LENGTH_SHORT).show();
                }

                if (userSchema.getValidity() == 1) {
                    saveUserDataOnSharedPreference(userSchema);
                    Intent intent = new Intent(MobileActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserSchema> call, Throwable t) {
                Toast.makeText(MobileActivity.this, "An Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("error", t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }


    /*
     * Save data in SharedPreference
     * */
    private void saveUserDataOnSharedPreference(UserSchema userSchema) {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Helpers.SHARED_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SaralGuru_Config.userID, userSchema.getUserId());
        editor.putInt(SaralGuru_Config.userValidity, userSchema.getValidity());
        editor.putString(SaralGuru_Config.userFirstName, userSchema.getFirstName());
        editor.putString(SaralGuru_Config.userLastName, userSchema.getLastName());
        editor.putString(SaralGuru_Config.userEmail, userSchema.getEmail());
        editor.putString(SaralGuru_Config.userRole, userSchema.getRole());
        editor.putString(SaralGuru_Config.userToken, userSchema.getToken());
        editor.putString(SaralGuru_Config.userPhoneNumber, userSchema.getMobile());
        editor.commit();
    }
}