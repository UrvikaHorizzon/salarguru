package com.horizzon.saralgurucourse.Models;

import com.google.gson.annotations.SerializedName;
import com.horizzon.saralgurucourse.JSONSchemas.Banner;

import java.util.ArrayList;

public class Banners {


    @SerializedName("heroes")
    private ArrayList<Banner> heros;

    public Banners(){

    }

    public ArrayList<Banner> getHeros(){
        return heros;
    }
}