package com.horizzon.saralgurucourse.Models;

public class video_details_db {

    private int id;
    private int uid;
    private int vid_id;
    private String video_name;
    private String video_path;


    public video_details_db(int uid, int vid_id, String video_name, String video_path) {
        this.uid = uid;
        this.vid_id = vid_id;
        this.video_name = video_name;
        this.video_path = video_path;
    }


    public video_details_db(int id, int uid, int vid_id, String video_name, String video_path) {
        this.id = id;
        this.uid = uid;
        this.vid_id = vid_id;
        this.video_name = video_name;
        this.video_path = video_path;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getVid_id() {
        return vid_id;
    }

    public void setVid_id(int vid_id) {
        this.vid_id = vid_id;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }



}
