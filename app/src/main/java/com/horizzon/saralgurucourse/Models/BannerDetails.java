package com.horizzon.saralgurucourse.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerDetails {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("banner_image")
    @Expose
    private String bannerImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }


}
