package com.horizzon.saralgurucourse.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class referal_code {

    @SerializedName("refferal_code")
    @Expose
    private String refferalCode;

    public String getRefferalCode() {
        return refferalCode;
    }

    public void setRefferalCode(String refferalCode) {
        this.refferalCode = refferalCode;
    }
}
