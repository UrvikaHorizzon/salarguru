package com.horizzon.saralgurucourse.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DownloadVideoInQuality {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("profile")
        @Expose
        private Integer profile;
        @SerializedName("width-By-Height")
        @Expose
        private String widthByHeight;
        @SerializedName("mime")
        @Expose
        private String mime;
        @SerializedName("fps")
        @Expose
        private Integer fps;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("cdn")
        @Expose
        private String cdn;
        @SerializedName("quality")
        @Expose
        private String quality;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("origin")
        @Expose
        private String origin;
        @SerializedName("filesize")
        @Expose
        private String filesize;

        public Integer getProfile() {
            return profile;
        }

        public void setProfile(Integer profile) {
            this.profile = profile;
        }

        public String getWidthByHeight() {
            return widthByHeight;
        }

        public void setWidthByHeight(String widthByHeight) {
            this.widthByHeight = widthByHeight;
        }

        public String getMime() {
            return mime;
        }

        public void setMime(String mime) {
            this.mime = mime;
        }

        public Integer getFps() {
            return fps;
        }

        public void setFps(Integer fps) {
            this.fps = fps;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCdn() {
            return cdn;
        }

        public void setCdn(String cdn) {
            this.cdn = cdn;
        }

        public String getQuality() {
            return quality;
        }

        public void setQuality(String quality) {
            this.quality = quality;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String getFilesize() {
            return filesize;
        }

        public void setFilesize(String filesize) {
            this.filesize = filesize;
        }

    }
}
