package com.horizzon.saralgurucourse.Models;

import java.io.Serializable;

public class CommentModelList implements Serializable {

    String comment,timeData;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTimeData() {
        return timeData;
    }

    public void setTimeData(String timeData) {
        this.timeData = timeData;
    }
}
