package com.horizzon.saralgurucourse.Utils;

public class SaralGuru_Config {

    public static final String appname="SaralGuru";
    public static final String userID="userId";
    public static final String userValidity="userValidity";
    public static final String userFirstName="userFirstName";
    public static final String userLastName="userLastName";
    public static final String userEmail="userEmail";
    public static final String userPhoneNumber="mobile_number";
    public static final String userRole="userRole";
    public static final String userToken="userToken";
    public static final String Status="status";
    public static final int PaymentSuccessful=1;
    public static final int FULLSCREEN_VIDEO=102;
    public static final String SHAREDPREFRANCE_VIDEO="SHRED_PREF_VIDEO";
    public static final String SHAREDPREFRANCE_VIDEO_NAME="SHRED_PREF_VIDEO_NAME";
    public static final String FULLSCREEN_VIDEO_ID="SHRED_PREF_VIDEO_ID";
}
