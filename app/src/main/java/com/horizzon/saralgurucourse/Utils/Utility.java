package com.horizzon.saralgurucourse.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Utility {

    public static boolean isdownoad;
    public static String getImeiNo(Context context) {
        String imei_number="";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            imei_number = Settings.Secure.getString(
                    context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Log.d("IMEI", imei_number);
        }else {
            int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);

            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 1111);
            } else {
                //TODO
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (null != tm) {
                    imei_number = tm.getDeviceId();
                }
                if (null == imei_number || 0 == imei_number.length()) {
                    imei_number = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.d("IMEI", imei_number);
                }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                Log.d("IMEI", imei_number);
            }
        }
        Log.d("IMEI", imei_number);

        return imei_number;
    }

    public static boolean isDownloding(boolean downoad){
        isdownoad=downoad;
        return downoad;
    }

    public static boolean getBoolean(){
        return isdownoad;
    }
}
