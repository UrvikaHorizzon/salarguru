package com.horizzon.saralgurucourse.JSONSchemas;

import com.google.gson.annotations.SerializedName;

public class Banner {



    @SerializedName("banner_image")
    private String url;

    public Banner(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}