package com.horizzon.saralgurucourse.JSONSchemas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalVideoLikeCount {

    @SerializedName("totalcount")
    @Expose
    private Integer totalcount;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
